Vue.component("registration", { 
	data: function () {
	    return {
			user: {},
			isValid: false
	    }
	},
	    template: `
	    <div>
            <form method="post" v-on:submit="validation()">
                <input type="text" placeholder="Username" name="username"><br>
                <input type="text" placeholder="Password" name="password"><br>
                <input type="text" placeholder="Confirm password" name="confirmedPassword"><br>
                <input type="text" placeholder="Name" name="name"><br>
                <input type="text" placeholder="Surname" name="surname"><br>
                <input type="radio" name="gender">Male
                <input type="radio" name="gender">Female<br>
                <input type="date" placeholder="Date of birth" name="birthday"><br>
                <input type="submit" value="Sign in" name="signIn"><br>
            </form>
        </div>
			`,
    mounted () {
        
    },
    methods: {
    	validation : function(){
			let confirmedPassword = document.getElementsByName("confirmedPassword")[0];
			let sex = document.getElementById("male")[0];
			if(confirmedPassword === this.user.password){
				isValid = true;
			}

			if(isValid){
				user.role = Role.Costumer;
				if(sex.checked){
					user.sex = Sex.Male;
				}
				else{
					user.sex = Sex.Female;
				}
				var s = {username:user.username,
						 password:user.password,
						 name:user.name,
						 surname:user.surname,
						 sex:user.sex,
						 dateOfBirth:user.dateOfBirth,
						 role:user.role,
						 rentals:null,
						 cart:null,
						 points:0,
						 type:null,
						 deleted:false}
				axios
				.post("rest/user/register", s)
				.then(response => toast('User' + user.name + ' ' + user.surname + 'successfully saved'));
			}
		}
    }
});