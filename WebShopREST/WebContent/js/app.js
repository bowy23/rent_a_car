const Registration = { template: '<registration></registration>' }

const router = new VueRouter({
	mode: 'hash',
	  routes: [
		{ path: '/', name: 'registration', component: Registration},
	  ]
});

var app = new Vue({
	router,
	el: '#element'
});