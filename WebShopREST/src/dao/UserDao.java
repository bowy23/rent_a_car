package dao;

import java.io.FileWriter;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import beans.*;
import java.util.List;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

public class UserDao {
    private List<User> users = new ArrayList<User>();

    Gson gson = new GsonBuilder().serializeNulls().create();

    private final String filePath = "C:\\Users\\Mili Bovan\\Desktop\\Web_Rent_a_car_final\\rent_a_car\\WebShopREST\\src\\data\\";

    public UserDao(){}
    public UserDao(String contextPath){}

    public List<User> getAllUsers(){
        return users;
    }

    public User findUser(String username, String password){
        for(User user : users){
            if(user.getUsername().equals(username) && user.getPassword().equals(password))
                return user;
        }
        return null;
    }

    public void addUser(User user){
        users.add(user);
    }

    public void loadUsers(){
        try{
            Reader reader = Files.newBufferedReader(Paths.get("users.json"));

            users = gson.fromJson(reader, new TypeToken<List<User>>(){}.getType());

            reader.close();
        } catch (Exception e){
            System.out.println(e);
            e.getStackTrace();
        }
    }

    public void saveUsers(){
        try{
            gson.toJson(users, new FileWriter(filePath + "user.json"));
        } catch (Exception e){
            System.out.println(e);
            e.getStackTrace();
        }
    }
}
