package beans;

import java.util.Objects;

public class Vehicle {
    public enum VehicleType {Car, Van, MobileHome, Motorcycle, Bus, Truck, Scooter}
    public enum Transmission {Manual, Automatic}
    public enum FuelType {Diesel, Gasoline, Hybrid, Electric}
    private int id;
    private String brand;
    private String model;
    private float price;
    private VehicleType type;
    private RentalFacility rentalFacility;
    private Transmission transmission;
    private FuelType fuelType;
    private double consumption;
    private int numberOfDoors;
    private int numberOfPassengers;
    private String description;
    private String picture;
    private Boolean isAvailable;
    private boolean deleted;

    public Vehicle() {
            super();
    }

    public Vehicle(String brand, String model, float price, VehicleType type, RentalFacility rentalFacility, Transmission transmission, FuelType fuelType, double consumption, int numberOfDoors, int numberOfPassengers, String description, String picture, Boolean isAvailable, boolean deleted) {
        super();
        this.brand = brand;
        this.model = model;
        this.price = price;
        this.type = type;
        this.rentalFacility = rentalFacility;
        this.transmission = transmission;
        this.fuelType = fuelType;
        this.consumption = consumption;
        this.numberOfDoors = numberOfDoors;
        this.numberOfPassengers = numberOfPassengers;
        this.description = description;
        this.picture = picture;
        this.isAvailable = isAvailable;
        this.deleted = deleted;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public VehicleType getType() {
        return type;
    }

    public void setType(VehicleType type) {
        this.type = type;
    }

    public RentalFacility getRentalFacility() {
        return rentalFacility;
    }

    public void setRentalFacility(RentalFacility rentalFacility) {
        this.rentalFacility = rentalFacility;
    }

    public Transmission getTransmission() {
        return transmission;
    }

    public void setTransmission(Transmission transmission) {
        this.transmission = transmission;
    }

    public FuelType getFuelType() {
        return fuelType;
    }

    public void setFuelType(FuelType fuelType) {
        this.fuelType = fuelType;
    }

    public double getConsumption() {
        return consumption;
    }

    public void setConsumption(double consumption) {
        this.consumption = consumption;
    }

    public int getNumberOfDoors() {
        return numberOfDoors;
    }

    public void setNumberOfDoors(int numberOfDoors) {
        this.numberOfDoors = numberOfDoors;
    }

    public int getNumberOfPassengers() {
        return numberOfPassengers;
    }

    public void setNumberOfPassengers(int numberOfPassengers) {
        this.numberOfPassengers = numberOfPassengers;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public Boolean getAvailable() {
        return isAvailable;
    }

    public void setAvailable(Boolean available) {
        isAvailable = available;
    }

    public boolean isDeleted() { return deleted;    }
    public void setDeleted(boolean deleted) { this.deleted = deleted;    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vehicle vehicle = (Vehicle) o;
        return id == vehicle.id && Float.compare(vehicle.price, price) == 0 && Double.compare(vehicle.consumption, consumption) == 0 && numberOfDoors == vehicle.numberOfDoors && numberOfPassengers == vehicle.numberOfPassengers && deleted == vehicle.deleted && brand.equals(vehicle.brand) && model.equals(vehicle.model) && type == vehicle.type && rentalFacility.equals(vehicle.rentalFacility) && transmission == vehicle.transmission && fuelType == vehicle.fuelType && description.equals(vehicle.description) && picture.equals(vehicle.picture) && isAvailable.equals(vehicle.isAvailable);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, brand, model, price, type, rentalFacility, transmission, fuelType, consumption, numberOfDoors, numberOfPassengers, description, picture, isAvailable, deleted);
    }

    @Override
    public String toString() {
        return "Vehicle{" +
                "id=" + id +
                ", brand='" + brand + '\'' +
                ", model='" + model + '\'' +
                ", price=" + price +
                ", type=" + type +
                ", rentalFacility=" + rentalFacility +
                ", transmission=" + transmission +
                ", fuelType=" + fuelType +
                ", consumption=" + consumption +
                ", numberOfDoors=" + numberOfDoors +
                ", numberOfPassengers=" + numberOfPassengers +
                ", description='" + description + '\'' +
                ", picture='" + picture + '\'' +
                ", isAvailable=" + isAvailable +
                ", deleted=" + deleted +
                '}';
    }
}
