package beans;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

public class Rental {
    public enum Status {Pending, Confirmed, Taken, Returned, Rejected, Cancelled}
    private String id;
    private List<Vehicle> vehicles;
    private RentalFacility rentalFacility;
    private LocalDateTime date;
    private String duration;//TODO
    private double price;
    private String buyerFullName;
    private Status status;

    public Rental() {
        super();
    }

    public Rental(List<Vehicle> vehicles, RentalFacility rentalFacility, LocalDateTime date, String duration, double price, String buyerFullName, Status status) {
        super();
        this.vehicles = vehicles;
        this.rentalFacility = rentalFacility;
        this.date = date;
        this.duration = duration;
        this.price = price;
        this.buyerFullName = buyerFullName;
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<Vehicle> getVehicles() {
        return vehicles;
    }

    public void setVehicles(List<Vehicle> vehicles) {
        this.vehicles = vehicles;
    }

    public RentalFacility getRentalFacility() {
        return rentalFacility;
    }

    public void setRentalFacility(RentalFacility rentalFacility) {
        this.rentalFacility = rentalFacility;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getBuyerFullName() {
        return buyerFullName;
    }

    public void setBuyerFullName(String buyerFullName) {
        this.buyerFullName = buyerFullName;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Rental{" +
                "vehicles=" + vehicles +
                ", rentalFacility=" + rentalFacility +
                ", date=" + date +
                ", duration='" + duration + '\'' +
                ", price=" + price +
                ", buyerFullName='" + buyerFullName + '\'' +
                ", status=" + status +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Rental rental = (Rental) o;
        return Double.compare(rental.price, price) == 0 && id.equals(rental.id) && vehicles.equals(rental.vehicles) && rentalFacility.equals(rental.rentalFacility) && date.equals(rental.date) && duration.equals(rental.duration) && buyerFullName.equals(rental.buyerFullName) && status == rental.status;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, vehicles, rentalFacility, date, duration, price, buyerFullName, status);
    }
}
