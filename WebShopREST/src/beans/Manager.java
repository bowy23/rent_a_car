package beans;

import java.time.LocalDate;
import java.util.Objects;

public class Manager extends User{
    private RentalFacility rentalFacility;
    private boolean deleted;

    public Manager() {
        super();
    }

    public Manager(String username, String password, String name, String surname, Sex sex, LocalDate dateOfBirth, Role role, RentalFacility rentalFacility, boolean deleted) {
        super(username, password, name, surname, sex, dateOfBirth, role);
        this.rentalFacility = rentalFacility;
        this.deleted = deleted;
    }

    public RentalFacility getRentalFacility() {
        return rentalFacility;
    }

    public void setRentalFacility(RentalFacility rentalFacility) {
        this.rentalFacility = rentalFacility;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Manager manager = (Manager) o;
        return deleted == manager.deleted && rentalFacility.equals(manager.rentalFacility);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), rentalFacility, deleted);
    }

    @Override
    public String toString() {
        return "Manager{" +
                "rentalFacility=" + rentalFacility +
                ", deleted=" + deleted +
                '}';
    }
}
