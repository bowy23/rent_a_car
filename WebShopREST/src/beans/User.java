package beans;

import java.time.LocalDate;
import java.util.Objects;

public class User {
    public enum Sex {Male, Female}
    public enum Role {Costumer, Manager, Admin}
    private String username;
    private String password;
    private String name;
    private String surname;
    private Sex sex;
    private LocalDate dateOfBirth;
    private Role role;

    public User() {
        super();
    }

    public User(String username, String password, String name, String surname, Sex sex, LocalDate dateOfBirth, Role role) {
        super();
        this.username = username;
        this.password = password;
        this.name = name;
        this.surname = surname;
        this.sex = sex;
        this.dateOfBirth = dateOfBirth;
        this.role = role;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "User{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", sex=" + sex +
                ", dateOfBirth=" + dateOfBirth +
                ", role=" + role +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return username.equals(user.username) && password.equals(user.password) && name.equals(user.name) && surname.equals(user.surname) && sex == user.sex && dateOfBirth.equals(user.dateOfBirth) && role == user.role;
    }

    @Override
    public int hashCode() {
        return Objects.hash(username, password, name, surname, sex, dateOfBirth, role);
    }
}
