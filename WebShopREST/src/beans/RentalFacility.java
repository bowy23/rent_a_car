package beans;

import java.util.Objects;

public class RentalFacility {
    private int id;
    private Vehicle rentingVehicles;
    private WorkingHours workingHours;
    private Boolean status;
    private Location location;
    private String logo;
    private double rating;
    private boolean deleted;

    public RentalFacility() {
        super();
    }

    public RentalFacility(Vehicle rentingVehicles, WorkingHours workingHours, Boolean status, Location location, String logo, double rating, boolean deleted) {
        super();
        this.rentingVehicles = rentingVehicles;
        this.workingHours = workingHours;
        this.status = status;
        this.location = location;
        this.logo = logo;
        this.rating = rating;
        this.deleted = deleted;
    }
    //TODO
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Vehicle getRentingVehicles() {
        return rentingVehicles;
    }

    public void setRentingVehicles(Vehicle rentingVehicles) {
        this.rentingVehicles = rentingVehicles;
    }

    public WorkingHours getWorkingHours() {
        return workingHours;
    }

    public void setWorkingHours(WorkingHours workingHours) {
        this.workingHours = workingHours;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RentalFacility that = (RentalFacility) o;
        return id == that.id && Double.compare(that.rating, rating) == 0 && deleted == that.deleted && rentingVehicles.equals(that.rentingVehicles) && workingHours.equals(that.workingHours) && status.equals(that.status) && location.equals(that.location) && logo.equals(that.logo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, rentingVehicles, workingHours, status, location, logo, rating, deleted);
    }

    @Override
    public String toString() {
        return "RentalFacility{" +
                "id=" + id +
                ", rentingVehicles=" + rentingVehicles +
                ", workingHours=" + workingHours +
                ", status=" + status +
                ", location=" + location +
                ", logo='" + logo + '\'' +
                ", rating=" + rating +
                '}';
    }
}
