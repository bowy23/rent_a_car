package beans;

import java.util.Objects;

public class CostumerType {
    public enum TypeName {Golden, Silver, Bronze}
    private TypeName typeName;
    private double discount;
    private int requiredPoints;

    public CostumerType() {
        super();
    }
    
    public CostumerType(TypeName typeName, double discount, int requiredPoints) {
        super();
        this.typeName = typeName;
        this.discount = discount;
        this.requiredPoints = requiredPoints;
    }

    public TypeName getTypeName() {
        return typeName;
    }

    public void setTypeName(TypeName typeName) {
        this.typeName = typeName;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public int getRequiredPoints() {
        return requiredPoints;
    }

    public void setRequiredPoints(int requiredPoints) {
        this.requiredPoints = requiredPoints;
    }

    @Override
    public String toString() {
        return "CostumerType{" +
                "typeName=" + typeName +
                ", discount=" + discount +
                ", requiredPoints=" + requiredPoints +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CostumerType that = (CostumerType) o;
        return Double.compare(that.discount, discount) == 0 && requiredPoints == that.requiredPoints && typeName == that.typeName;
    }

    @Override
    public int hashCode() {
        return Objects.hash(typeName, discount, requiredPoints);
    }

}
