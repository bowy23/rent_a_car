package beans;

import java.time.LocalDate;

public class Admin extends User{
    public Admin() {
        super();
    }

    public Admin(String username, String password, String name, String surname, Sex sex, LocalDate dateOfBirth, Role role) {
        super(username, password, name, surname, sex, dateOfBirth, role);
    }

}
