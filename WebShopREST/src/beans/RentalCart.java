package beans;

import java.util.List;
import java.util.Objects;

public class RentalCart {
    private List<Vehicle> vehicles;
    private Costumer buyer;
    private double price;
    private boolean deleted;

    public RentalCart() {
        super();
    }

    public RentalCart(List<Vehicle> vehicles, Costumer buyer, double price, boolean deleted) {
        super();
        this.vehicles = vehicles;
        this.buyer = buyer;
        this.price = price;
        this.deleted = deleted;
    }

    public List<Vehicle> getVehicles() {
        return vehicles;
    }

    public void setVehicles(List<Vehicle> vehicles) {
        this.vehicles = vehicles;
    }

    public Costumer getBuyer() {
        return buyer;
    }

    public void setBuyer(Costumer buyer) {
        this.buyer = buyer;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    public String toString() {
        return "RentalCart{" +
                "vehicles=" + vehicles +
                ", buyer=" + buyer +
                ", price=" + price +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RentalCart that = (RentalCart) o;
        return Double.compare(that.price, price) == 0 && deleted == that.deleted && vehicles.equals(that.vehicles) && buyer.equals(that.buyer);
    }

    @Override
    public int hashCode() {
        return Objects.hash(vehicles, buyer, price, deleted);
    }
}
