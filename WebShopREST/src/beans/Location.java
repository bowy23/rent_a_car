package beans;

import java.util.Objects;

public class Location {
    private double longitude; //duzina
    private double latitude; //sirina
    private Address address;
    private boolean deleted;

    public Location() {
        super();
    }

    public Location(double longitude, double latitude, Address address, boolean deleted) {
        super();
        this.longitude = longitude;
        this.latitude = latitude;
        this.address = address;
        this.deleted = deleted;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Location location = (Location) o;
        return Double.compare(location.longitude, longitude) == 0 && Double.compare(location.latitude, latitude) == 0 && deleted == location.deleted && Objects.equals(address, location.address);
    }

    @Override
    public int hashCode() {
        return Objects.hash(longitude, latitude, address, deleted);
    }

    @Override
    public String toString() {
        return "Location{" +
                "longitude=" + longitude +
                ", latitude=" + latitude +
                ", address=" + address +
                ", deleted=" + deleted +
                '}';
    }
}
