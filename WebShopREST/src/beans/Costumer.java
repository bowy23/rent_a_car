package beans;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

public class Costumer extends User {
    private List<Rental> rentals;
    private RentalCart cart;
    private int points;
    private CostumerType type;

    private boolean deleted;

    public Costumer() {
        super();
    }

    public Costumer(String username, String password, String name, String surname, Sex sex, LocalDate dateOfBirth, Role role, List<Rental> rentals, RentalCart cart, int points, CostumerType type, boolean deleted) {
        super(username, password, name, surname, sex, dateOfBirth, role);
        this.rentals = rentals;
        this.cart = cart;
        this.points = points;
        this.type = type;
        this.deleted = deleted;
    }

    public List<Rental> getRentals() {
        return rentals;
    }

    public void setRentals(List<Rental> rentals) {
        this.rentals = rentals;
    }

    public RentalCart getCart() {
        return cart;
    }

    public void setCart(RentalCart cart) {
        this.cart = cart;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public CostumerType getType() {
        return type;
    }

    public void setType(CostumerType type) {
        this.type = type;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Costumer costumer = (Costumer) o;
        return points == costumer.points && deleted == costumer.deleted && rentals.equals(costumer.rentals) && cart.equals(costumer.cart) && type.equals(costumer.type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), rentals, cart, points, type, deleted);
    }

    @Override
    public String toString() {
        return "Costumer{" +
                "rentals=" + rentals +
                ", cart=" + cart +
                ", points=" + points +
                ", type=" + type +
                ", deleted=" + deleted +
                '}';
    }
}
