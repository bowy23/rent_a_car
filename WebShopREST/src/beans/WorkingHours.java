package beans;

import java.time.LocalTime;
import java.util.Objects;

public class WorkingHours {
    private LocalTime startingHour;
    private LocalTime closingHour;
    private boolean deleted;

    public WorkingHours() {
        super();
    }

    public WorkingHours(LocalTime startingHour, LocalTime closingHour, boolean deleted) {
        super();
        this.startingHour = startingHour;
        this.closingHour = closingHour;
        this.deleted = deleted;
    }

    public LocalTime getStartingHour() {
        return startingHour;
    }

    public void setStartingHour(LocalTime startingHour) {
        this.startingHour = startingHour;
    }

    public LocalTime getClosingHour() {
        return closingHour;
    }

    public void setClosingHour(LocalTime closingHour) {
        this.closingHour = closingHour;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    public String toString() {
        return "WorkingHours{" +
                "startingHour=" + startingHour +
                ", closingHour=" + closingHour +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WorkingHours that = (WorkingHours) o;
        return deleted == that.deleted && startingHour.equals(that.startingHour) && closingHour.equals(that.closingHour);
    }

    @Override
    public int hashCode() {
        return Objects.hash(startingHour, closingHour, deleted);
    }
}
