package beans;

import java.util.Objects;

public class Address {
    private String street;
    private String number;
    private String city;
    private long postalCode;

    private boolean deleted;
    public Address() {
        super();
    }

    public Address(String street, String number, String city, long postalCode, boolean deleted) {
        this.street = street;
        this.number = number;
        this.city = city;
        this.postalCode = postalCode;
        this.deleted = deleted;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public long getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(long postalCode) {
        this.postalCode = postalCode;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    public String toString() {
        return "Address{" +
                "street='" + street + '\'' +
                ", number='" + number + '\'' +
                ", city='" + city + '\'' +
                ", postalCode=" + postalCode +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Address address = (Address) o;
        return postalCode == address.postalCode && deleted == address.deleted && street.equals(address.street) && number.equals(address.number) && city.equals(address.city);
    }

    @Override
    public int hashCode() {
        return Objects.hash(street, number, city, postalCode, deleted);
    }
}
