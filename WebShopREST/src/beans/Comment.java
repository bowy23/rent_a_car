package beans;

import java.util.Objects;

public class Comment {
    private Costumer costumer;
    private RentalFacility rentalFacility;
    private String comment;
    private int rating;
    private boolean deleted;

    public Comment() {
        super();
    }

    public Comment(Costumer costumer, RentalFacility rentalFacility, String comment, int rating, boolean deleted) {
        super();
        this.costumer = costumer;
        this.rentalFacility = rentalFacility;
        this.comment = comment;
        this.rating = rating;
        this.deleted = deleted;
    }

    public Costumer getCostumer() {
        return costumer;
    }

    public void setCostumer(Costumer costumer) {
        this.costumer = costumer;
    }

    public RentalFacility getRentalFacility() {
        return rentalFacility;
    }

    public void setRentalFacility(RentalFacility rentalFacility) {
        this.rentalFacility = rentalFacility;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    public String toString() {
        return "Comment{" +
                "costumer=" + costumer +
                ", rentalFacility=" + rentalFacility +
                ", comment='" + comment + '\'' +
                ", rating=" + rating +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Comment comment1 = (Comment) o;
        return rating == comment1.rating && deleted == comment1.deleted && costumer.equals(comment1.costumer) && rentalFacility.equals(comment1.rentalFacility) && comment.equals(comment1.comment);
    }

    @Override
    public int hashCode() {
        return Objects.hash(costumer, rentalFacility, comment, rating, deleted);
    }
}
