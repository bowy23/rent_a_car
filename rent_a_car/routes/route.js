const express = require("express");
const router = express.Router();
const userService = require("../services/userService");
const rentacarService = require("../services/rentacarService");
const vehicleService = require("../services/vehicleService");
const orderService = require("../services/orderService");
const commentService = require("../services/commentService");

router.get('/registration', async function(req, res, next) {
    try {
      res.send('OK');
    } catch (err) {
      console.error('Error while doing test: ', err.message);
      next(err);
    }
});

router.get('/logedInUser', async function(req, res, next) {
  try {
    const logedInUser = userService.getLoggedIn();
    res.json(logedInUser);
  } catch (err) {
    console.error('Error while doing test: ', err.message);
    next(err);
  }
});

router.post('/register', async function(req, res, next) {
    try {
      let user = req.body;
      userService.register(user);
      res.json('OK');
    } catch (err) {
      console.error('Error: ', err.message);
      next(err);
    }
  });

router.post('/login', async function(req, res, next) {
  try {
    let username = req.body.username;
    let password = req.body.password;
    userService.login(username, password);
    const user = userService.getLoggedIn();
    res.json(user);
  } catch (err) {
    console.error('Error: ', err.message);
    next(err);
  }
});

router.get('/logout', async function(req, res, next) {
  try {
    userService.logout();
  } catch (err) {
    console.error('Error while doing test: ', err.message);
    next(err);
  }
});

router.get('/getRentacars', async function(req, res, next) {
  try {
    const rents = await rentacarService.getRents();
    res.json(rents)
  } catch (err) {
    console.error('Error while doing test: ', err.message);
    next(err);
  }
});

router.get('/getUsers', async function(req, res, next) {
  try {
    const users = await userService.getUsers();
    res.json(users)
  } catch (err) {
    console.error('Error while doing test: ', err.message);
    next(err);
  }
});

router.get('/getRentacar/:name', async function(req, res, next) {
  try {
    const name  = req.params;
    const rentacar = await rentacarService.getRentacar(name.name);
    res.json(rentacar)
  } catch (err) {
    console.error('Error while doing test: ', err.message);
    next(err);
  }
});

router.post('/createObject', async function(req, res, next) {
  try {
    let rentacar = req.body;
    rentacarService.createObject(rentacar);
    res.json(rentacar);
  } catch (err) {
    console.error('Error: ', err.message);
    next(err);
  }
});

router.put('/update', async function(req, res, next) {
  try {
    userService.updateUser(req.body);
    res.json('OK');
  } catch (err) {
    console.error('Error: ', err.message);
    next(err);
  }
});

router.post('/addVehicle/:name', async function(req, res, next) {
  try {
    let name = req.params;
    let vehicle = req.body;
    vehicleService.addVehicle(vehicle);
    rentacarService.addVehicle(name.name, vehicle);
    res.json("OK");
  } catch (err) {
    console.error('Error: ', err.message);
    next(err);
  }
});

router.delete('/deleteVehicle/:brand/:model', async function(req, res, next) {
  try {
    let {brand, model} = req.params;
    vehicleService.deleteVehicle(brand, model);
    rentacarService.deleteVehicle(brand, model);
    res.json("OK");
  } catch (err) {
    console.error('Error: ', err.message);
    next(err);
  }
});

router.put('/updateVehicle', async function(req, res, next) {
  try {
    let vehicle = req.body;
    console.log(vehicle);
    vehicleService.updateVehicle(vehicle.v);
    rentacarService.updateVehicle(vehicle.v);
    res.json("OK");
  } catch (err) {
    console.error('Error: ', err.message);
    next(err);
  }
});

router.get('/getAvailableVehicles', async function(req, res, next) {
  try {
    const vehicles = await vehicleService.getAvailableVehicles();
    res.json(vehicles);
  } catch (err) {
    console.error('Error: ', err.message);
    next(err);
  }
});

router.get('/getAvailableForDate', async function(req, res, next) {
  try {
    const { date } = req.body;
    const orders = await orderService.getOrders();
    const availableVehicles = await vehicleService.getAvailableVehicles();

    // Filter out vehicles that are booked for the given date
    // Dates
    const unavailableVehicles = orders
      .filter((order) => order.status === 'Pending')
      .flatMap((order) => order.vehicles)
      .map((vehicle) => `${vehicle.brand}-${vehicle.model}`); // Assuming a unique identifier for vehicles

    const filteredVehicles = availableVehicles.filter((vehicle) => {
      const vehicleIdentifier = `${vehicle.brand}-${vehicle.model}`;
      return !unavailableVehicles.includes(vehicleIdentifier);
    });

    res.json(filteredVehicles);
  } catch (err) {
    console.error('Error: ', err.message);
    next(err);
  }
});

router.get('/getAllOrders', async function(req, res, next) {
  try {
    const orders = await orderService.getOrders();
    res.json(orders);
  } catch (err) {
    console.error('Error: ', err.message);
    next(err);
  }
});

router.get('/getAllOrders/:username', async function(req, res, next) {
  try {
    const username = req.params;
    const orders = await orderService.getOrdersByUser(username);
    res.json(orders);
  } catch (err) {
    console.error('Error: ', err.message);
    next(err);
  }
});

router.post('/createOrder', async function(req, res, next){
  try {
    const order = req.body
    orderService.createOrder(order);
  } catch (error) {
    console.error('Error: ', err.message);
    next(err);
  }
});

router.put('/updateOrder', async function(req, res, next) {
  try {
    let order = req.body
    orderService.updateOrder(order.updatedOrder);
    res.json('OK');
  } catch (err) {
    console.error('Error: ', err.message);
    next(err);
  }
});

router.post('/createComment', async function(req, res, next){
  try {
    const comment = req.body
    console.log(comment)
    commentService.createComment(comment);
  } catch (error) {
    console.error('Error: ', err.message);
    next(err);
  }
});

router.get('/getAllComments', async function(req, res, next) {
  try {
    const comments = await commentService.getComments();
    res.json(comments);
  } catch (err) {
    console.error('Error: ', err.message);
    next(err);
  }
});

router.put('/updateComment', async function(req, res, next) {
  try {
    let comment = req.body
    commentService.updateComment(comment.comment);
    res.json('OK');
  } catch (err) {
    console.error('Error: ', err.message);
    next(err);
  }
});

router.get('/getOrdersByObject/:rentacarObject', async function(req, res, next) {
  try {
    let rentacarObject = req.params
    const orders = await orderService.getOrdersByObject(rentacarObject.rentacarObject);
    res.json(orders);
  } catch (err) {
    console.error('Error: ', err.message);
    next(err);
  }
});

router.get('/getCommentsForObject/:rentacarObject', async function(req, res, next) {
  try {
    let rentacarObject = req.params
    const comments = await commentService.getCommentsForObject(rentacarObject.rentacarObject);
    res.json(comments);
  } catch (err) {
    console.error('Error: ', err.message);
    next(err);
  }
});

module.exports = router;