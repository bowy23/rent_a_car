const express = require('express');
const serveStatic = require('serve-static');
const serveIndex = require('serve-index');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const sessions = require('express-session');
const cors = require("cors");

const app = express();
const port = 8081;

const corsOptions = {
    origin: 'http://localhost:8081',
    credentials: true,
  };
  
  app.use(cors(corsOptions)); 

app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(sessions({
    secret: "mycomplexsecretkeymakeitverycomplex",
    cookie: { maxAge: 1000 * 60 * 60 * 24 },
    saveUninitialized: false,
    resave: false
}));

/* Setup routes */
const router = require('./routes/route');
/* Setup URL->Route mapping */
app.use('/rest/', router);

/* Static content middleware */
app.use(serveStatic(__dirname + '/static')).use(serveIndex(__dirname + '/static'));

/* Start listening at the given port */
app.listen(port);
console.log('Server listening at http://localhost:8081');