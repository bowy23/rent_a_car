const Fuel = {
    Diesel: 'Diesel',
    Petrol: 'Petrol',
    Hybrid: 'Hybrid',
    Electric: 'Electric'
}

const Type = {
    Car: "Car",
    Van: 'Van',
    Bike: 'Bike',
    Quad: 'Quad',
    Mobilehome: 'Mobilehome'
}

const Transmission = {
    Manual: 'Manual',
    Automatic: 'Automatic'
}

const Status = {
    Available: 'Available',
    Rented: 'Rented'
}

Vue.component("editVehicles", { 
    data:function(){
        return{
            rentacar: {},
            add: false,
            edit: false,
            delete: false,
            vehicle: {
                brand: '',
                model: '',
                price: '',
                type: '',
                rentACarObject: '',
                transmission: '',
                fuel: '',
                consumption: '',
                doors: '',
                passengers: '',
                description: '',
                picture: '',
                status: '',
            },
            editedVehicle:{}
        }
    },

	template: `
    <div>
        <button v-on:click="addVehicle()">Add vehicle</button>
        <div v-if="this.add">
            <form v-on:submit="register()">
                <input v-model="vehicle.brand" type="text" placeholder="Brand" name="brand"><br>
                <input v-model="vehicle.model" type="text" placeholder="Model" name="model"><br>
                <input v-model="vehicle.price" type="number" placeholder="Price" name="price"><br>
                <select v-model="vehicle.type" name="type" id="type">
                    <option value="car">Car</option>
                    <option value="van">Van</option>
                    <option value="bike">Bike</option>
                    <option value="quad">Quad</option>
                    <option value="mobilehome">Mobilehome</option>
                </select>
                <select v-model="vehicle.fuel" name="fuel" id="fuel">
                    <option value="diesel">Diesel</option>
                    <option value="petrol">Petrol</option>
                    <option value="hybrid">Hybrid</option>
                    <option value="electric">Electric</option>
                </select>
                <select v-model="vehicle.transmission" name="transmission" id="transmission">
                    <option value="manual">Manual</option>    
                    <option value="automatic">Automatic</option>    
                </select><br>
                <input v-model="vehicle.consumption" type="number" placeholder="Consumption" name="consumption"><br>
                <input v-model="vehicle.doors" type="number" placeholder="Doors" name="doors"><br>
                <input v-model="vehicle.passengers" type="number" placeholder="Passengers" name="passengers"><br>
                <input v-model="vehicle.picture" type="text" placeholder="Picture" name="picture"><br>
                <input v-model="vehicle.description" type="text" placeholder="Description" name="description"><br>
                <input type="submit" value="Add vehicle" name="addVehicle">
            </form>
        </div>
        <div>
            <p><strong>Vehicles</strong></p>
            <ul>
                <li v-for="(vehicle, index) in rentacar.vehicles" :key="index">
                    <p><strong>Type:</strong> {{ vehicle.type }}</p>
                    <p><strong>Fuel:</strong> {{ vehicle.fuel }}</p>
                    <p><strong>Brand:</strong> {{ vehicle.brand }}</p>
                    <p><strong>Model:</strong> {{ vehicle.model }}</p>
                    <p><strong>Type:</strong> {{ vehicle.type }}</p>
                    <p><strong>Transmission:</strong> {{ vehicle.transmission }}</p>
                    <p><strong>Price:</strong> {{ vehicle.price }}</p>
                    <p><strong>Consumption:</strong> {{ vehicle.consumption }}</p>
                    <p><strong>Doors:</strong> {{ vehicle.doors }}</p>
                    <p><strong>Passenger:</strong> {{ vehicle.passengers }}</p>
                    <p><strong>Description:</strong> {{ vehicle.description }}</p>
                    <p><strong>Picture:</strong> {{ vehicle.picture }}</p>
                    <p><strong>Status:</strong> {{ vehicle.status }}</p>
                    <button v-on:click="editOn(vehicle)">Edit vehicle</button>
                    <button v-on:click="deleteVehicle(vehicle.brand, vehicle.model)">Delete vehicle</button>
                    </li>
            </ul>   
        </div>
        <div v-if="this.edit">
            <form v-on:submit="editVehicle()">
                <input v-model="editedVehicle.brand" type="text" placeholder="Brand" name="brand"><br>
                <input v-model="editedVehicle.model" type="text" placeholder="Model" name="model"><br>
                <input v-model="editedVehicle.price" type="number" placeholder="Price" name="price"><br>
                <select v-model="editedVehicle.type" name="type" id="type">
                    <option value="car">Car</option>
                    <option value="van">Van</option>
                    <option value="bike">Bike</option>
                    <option value="quad">Quad</option>
                    <option value="mobilehome">Mobilehome</option>
                </select>
                <select v-model="editedVehicle.fuel" name="fuel" id="fuel">
                    <option value="diesel">Diesel</option>
                    <option value="petrol">Petrol</option>
                    <option value="hybrid">Hybrid</option>
                    <option value="electric">Electric</option>
                </select>
                <select v-model="editedVehicle.transmission" name="transmission" id="transmission">
                    <option value="manual">Manual</option>    
                    <option value="automatic">Automatic</option>    
                </select><br>
                <input v-model="editedVehicle.consumption" type="number" placeholder="Consumption" name="consumption"><br>
                <input v-model="editedVehicle.doors" type="number" placeholder="Doors" name="doors"><br>
                <input v-model="editedVehicle.passengers" type="number" placeholder="Passengers" name="passengers"><br>
                <input v-model="editedVehicle.picture" type="text" placeholder="Picture" name="picture"><br>
                <input v-model="editedVehicle.description" type="text" placeholder="Description" name="description"><br>
                <input type="submit" value="Update vehicle" name="addVehicle">
            </form>
        </div>
    </div>  
    `,
    mounted(){
        const name = this.$route.params.name;
        this.fetchRentacarDetails(name);
    },
    methods: {
        fetchRentacarDetails(name){
            axios
            .get(`rest/getRentacar/${name}`)
            .then((response) => {
                console.log(response.data)
                this.rentacar = response.data
            })
            .catch((error) => {
                console.error('Error fetching rentacar details:', error);
            })
        },
        addVehicle(){
            this.add = true;
            this.edit = false;
            this.delete = false;
        },
        editOn(vehicle){
            this.edit = true;
            this.add = false;
            this.delete = false;
            this.editedVehicle = vehicle
        },
        deleteVehicle(brand, model){
            this.delete = true;
            this.add = false;
            this.edit = false;
            axios
            .delete(`rest/deleteVehicle/${brand}/${model}`)
            .then((response) => toast('Vehicle ' + model + ' ' + brand + ' successfully removed'))
            .catch((error) => console.error(error));

        },
        register() {
            let consumpt = ""; 
            if (this.vehicle.fuel === Fuel.Electric) {
                consumpt = "km";
            } else {
                consumpt = "l/100";
            }
        
            var Vehicle = {
                brand: this.vehicle.brand,
                model: this.vehicle.model,
                price: this.vehicle.price,
                type: this.vehicle.type,
                rentACarObject: this.rentacar.name,
                transmission: this.vehicle.transmission,
                fuel: this.vehicle.fuel,
                consumption: this.vehicle.consumption + consumpt,
                doors: this.vehicle.doors,
                passengers: this.vehicle.passengers,
                description: this.vehicle.description,
                picture: this.vehicle.picture,
                status: "available",
                deleted: false
            };
        
            const name = this.rentacar.name;
            console.log(Vehicle);
            axios
                .post(`rest/addVehicle/${name}`, { Vehicle })
                .then((response) => toast('Vehicle ' + this.vehicle.model + ' ' + this.vehicle.brand + ' successfully saved'))
                .catch((error) => console.error(error));
        },
        editVehicle() {
            let consumpt = ""; 
            if (this.editedVehicle.fuel === Fuel.Electric) {
                consumpt = "km";
            } else {
                consumpt = "l/100";
            }
        
            var v = {
                brand: this.editedVehicle.brand,
                model: this.editedVehicle.model,
                price: this.editedVehicle.price,
                type: this.editedVehicle.type,
                rentACarObject: this.rentacar.name,
                transmission: this.editedVehicle.transmission,
                fuel: this.editedVehicle.fuel,
                consumption: this.editedVehicle.consumption + consumpt,
                doors: this.editedVehicle.doors,
                passengers: this.editedVehicle.passengers,
                description: this.editedVehicle.description,
                picture: this.editedVehicle.picture,
                status: "available",
                deleted: false
            };
        
            axios
                .put(`rest/updateVehicle`, { v })
                .then((response) => toast('Vehicle ' + this.editedVehicle.model + ' ' + this.editedVehicle.brand + ' successfully saved'))
                .catch((error) => console.error(error));
        }
    }
});