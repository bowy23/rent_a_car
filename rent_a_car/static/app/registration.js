const Role = {
    Costumer: 'Costumer',
    Manager: 'Manager',
    Admin: 'Admin'
}
Vue.component("registration", { 
	data: function () {
	    return {
			user: {
                username: "",
                password: "",
                name: "",
                surname: "",
                gender: "",
                birthDate: null,
            },
            confirmedPassword: "",
			isValid: false
	    }
	},
	    template: `
	    <div>
            <form v-on:submit="validation()">
                <input type="text" v-model="user.username" placeholder="Username" name="username"><br>
                <input type="password" v-model="user.password" placeholder="Password" name="password"><br>
                <input type="password" v-model="confirmedPassword" placeholder="Confirm password" name="confirmedPassword"><br>
                <input type="text" v-model="user.name" placeholder="Name" name="name"><br>
                <input type="text" v-model="user.surname" placeholder="Surname" name="surname"><br>
                <input type="radio" v-model="user.gender" name="gender" value="Male">Male
                <input type="radio" v-model="user.gender" name="gender" value="Female">Female<br>
                <input type="date" v-model="user.birthDate" placeholder="Date of birth" name="birthday"><br>
                <input type="submit" value="Sign in" name="signIn"><br>
            </form>
        </div>
			`,
    methods: {
    	validation : function(){
			if(this.confirmedPassword === this.user.password){
				this.isValid = true;
                var user = {
                username: this.user.username,
                password: this.user.password,
                firstName: this.user.name,
                lastName: this.user.surname,
                gender: this.user.gender,
                birthDate: this.user.birthDate,
                role: Role.Costumer,
                allRentals: [],
                shoppingCart: [],
                rewardPoints: 0,
                costumerType: null,
                deleted: false,
                };

             axios.post("rest/register", {user},)
   .then((response) => toast('User ' + this.user.name + ' ' + this.user.surname + ' successfully saved'))
   .catch((error) => console.error(error));
                this.$router.push("/");
                
            }
            
		},
    }
});