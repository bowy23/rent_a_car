Vue.component("createRentacar", { 
    data:function(){
        return{
            rentacar:{
                name: "",
                startHour: "",
                endHour: "",
                location: "",
                logo: ""
            }
        }
    },

	template: `
    <div>
        <form v-on:submit="createObject()">    
            <input v-model="rentacar.name" type="text" placeholder="Name"><br>
            <input v-model="rentacar.location" vehicles type="text" placeholder="Location"><br>
            <input v-model="rentacar.startHour" type="time" placeholder="Start time">
            <input v-model="rentacar.endHour" type="time" placeholder="End time"><br>
            <input v-model="rentacar.logo" type="text" placeholder="logo"><br>
            <input type="submit" value="create" name="Create">
        </form>
    </div>
    `,
    methods: {
        createObject() {
            var rentACar = {
                name: this.rentacar.name,
                vehicles: [],
                workingHours: this.rentacar.startHour + " - " + this.rentacar.endHour,
                status: "Open",
                location: this.rentacar.location,
                logo: this.rentacar.logo,
                rating: 0
            }
            axios
            .post("rest/createObject", {rentACar})
            .then((response) => toast('Object ' + this.rentacar.name + ' successfully saved'))
   .catch((error) => console.error(error));
                this.$router.push("/");
        }
    }
});