Vue.component("manager", { 
    data:function(){
        return{
            manager:{
                username: "",
                password: "",
                firstName: "",
                lastName: "",
                gender: "",
                birthDate: "",
                role: "Manager",
                rentalAgency: ""
            },
            confirmedPassword: "",
            isValid: ""
        }
    },

	template: `
    <div>
    <form v-on:submit="validation()">
        <input type="text" v-model="manager.username" placeholder="Username" name="username"><br>
        <input type="password" v-model="manager.password" placeholder="Password" name="password"><br>
        <input type="password" v-model="confirmedPassword" placeholder="Confirm password" name="confirmedPassword"><br>
        <input type="text" v-model="manager.name" placeholder="Name" name="name"><br>
        <input type="text" v-model="manager.surname" placeholder="Surname" name="surname"><br>
        <input type="radio" v-model="manager.gender" name="gender" value="Male">Male
        <input type="radio" v-model="manager.gender" name="gender" value="Female">Female<br>
        <input type="date" v-model="manager.birthDate" placeholder="Date of birth" name="birthday"><br>
        <input type="text" v-model="manager.rentalAgency" placeholder="Agency" name="name"><br>
        <input type="submit" value="Sign in" name="signIn"><br>
    </form>
</div>
    `,
    methods: {
        validation : function(){
			if(this.confirmedPassword === this.manager.password){
				this.isValid = true;
                var user = {
                username: this.manager.username,
                password: this.manager.password,
                firstName: this.manager.name,
                lastName: this.manager.surname,
                gender: this.manager.gender,
                birthDate: this.manager.birthDate,
                role: Role.Manager,
                rentalAgency: this.manager.rentalAgency,
                deleted: false,
                };

             axios.post("rest/register", {user},)
   .then((response) => toast('User ' + this.user.name + ' ' + this.user.surname + ' successfully saved'))
   .catch((error) => console.error(error));
                this.$router.push("/");
                
            }
            
		},
    }
});