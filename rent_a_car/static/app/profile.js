const commentStatus = {
  Pending: 'Pending',
  Approved: 'Approved',
  Rejected: 'Rejected'
}

Vue.component("profile", {
  data: function () {
    return {
      user: {},
      isValid: false,
      users: [],
      orders: [],
      searchAdminCriteria: {
        firstName: '',
        lastName: '',
        username: ''
      },
      sortAdminCriteria: {
        firstName: '',
        lastName: '',
        username: '',
        rewardPoints: 0
      },
      filterAdminCriteria: {
        role
      },
      searchCriteria: {
        rentacarObject: "",
        priceLow: "",
        priceUp: "",
        startDate: "",
        endDate: "",
      },
      sortCriteria: {
        rentacarObject: "",
        price: "",
        date: "",
      },
      filterCriteria: {
        rentacarObject: "",
        price: "",
        date: "",
      },
      searchedOrders: [],
      searchedUsers: [],
      isCommented: false,
      commentRent: false,
      comment:{
        user: {},
        rentacarObject: [],
        text: "",
        rating: 1,
        commentStatus: commentStatus.Pending
      },
      comments: []
    };
  },
  computed: {
    isAdmin: function () {
      return this.user.role === "Admin";
    },
    isCostumer: function () {
      return this.user.role === "Costumer";
    },
    isManager: function () {
      if (this.user.role === "Manager") {
        return this.orders.some((order) =>
          order.rentacarObject.includes(this.user.rentalAgency)
        );
      }
      return false;
    },
    loggedInUser(){
      return this.user
    },
    formattedBirthDate: function () {
      if (this.user.birthDate) {
        const date = new Date(this.user.birthDate);
        const options = { year: "numeric", month: "2-digit", day: "2-digit" };
        return date.toLocaleDateString("en-CA", options);
      }
      return "";
    },
  },

  template: `
  <div>
  <form v-on:submit="validation()">
    <input
      type="text"
      v-model="user.username"
      placeholder="Username"
      name="username"
    /><br />
    <input
      type="password"
      v-model="user.password"
      placeholder="Password"
      name="password"
    /><br />
    <input
      type="text"
      v-model="user.firstName"
      placeholder="Name"
      name="name"
    /><br />
    <input
      type="text"
      v-model="user.lastName"
      placeholder="Surname"
      name="surname"
    /><br />
    <input type="radio" v-model="user.gender" name="gender" value="Male" />Male
    <input
      type="radio"
      v-model="user.gender"
      name="gender"
      value="Female"
    />Female<br />
    <input
      type="date"
      :value="formattedBirthDate"
      placeholder="Date of birth"
      name="birthday"
    /><br />
    <input type="submit" value="Update profile" name="update" /><br />
  </form>
  <div>
    <h2 v-if="isAdmin">User List</h2>
    <ul v-if="isAdmin">
      <li v-for="(user, index) in users" :key="index">
        {{ user.user.username }} - {{ user.user.role }}
      </li>
    </ul>
  </div>
  <div v-if="!isAdmin">
      <input type="text" v-if="isCostumer" v-model="searchCriteria.rentacarObject" placeholder="rentacarObject">
      <button v-if="isCostumer" v-on:click="searchByName()">Search by name</button><br>
      <input type="number" v-model="searchCriteria.priceLow" placeholder="priceLow">
      <input type="number" v-model="searchCriteria.priceUp" placeholder="priceUp">
      <button v-on:click="searchByPrice()">Search by price</button><br>
      <input type="date" v-model="searchCriteria.startDate" placeholder="startDate">
      <input type="date" v-model="searchCriteria.endDate" placeholder="priceUp">
      <button v-on:click="searchByDate()">Search by date</button><br>
  </div>
  <div>
    <button v-on:click="sortBy('rentacarObject')">Sort by rentacar object</button>
    <button v-on:click="sortBy('totalPrice')">Sort by price</button>
    <button v-on:click="sortBy('startDate')">Sort by date</button>
</div>
  <div v-if="isCostumer">
    <div v-for="(order, index) in searchedOrders" :key="index">
      <h2>Order {{ order.id }}</h2>
      <p><strong>Start Date:</strong> {{ order.startDate }}</p>
      <p><strong>Duration:</strong> {{ order.duration }} days</p>
      <p><strong>Total Price:</strong> {{ order.totalPrice }}</p>
      <p><strong>User:</strong> {{ order.user }}</p>
      <p><strong>Status:</strong> {{ order.orderStatus }}</p>
      <button v-if="order.orderStatus === 'Pending' && isCostumer" v-on:click="cancelOrder(order)">Cancel order</button>
      <button v-if="order.orderStatus === 'Returned' && isCostumer" v-on:click="commentRental()">Comment rental</button>
      <div v-if="commentRent">
        <form v-on:submit="commentIt(order.rentacarObject)">
          <input type="text" placeholder="Comment" v-model="comment.text"><br>
          <input type="number"  min="1" max="5" placeholder="Rate" v-model="comment.rating"><br>
          <input type="submit" value="Comment"><br>
        </form>
      </div>
      <div>
        <h3>Vehicles</h3>
        <div v-for="(vehicle, vIndex) in order.vehicles" :key="vIndex">
          <p><strong>Brand:</strong> {{ vehicle.brand }}</p>
          <p><strong>Model:</strong> {{ vehicle.model }}</p>
          <p><strong>Price:</strong> {{ vehicle.totalPrice }}</p>
          <p><strong>Type:</strong> {{ vehicle.type }}</p>
          <!-- Add other vehicle properties as needed -->
          <p><strong>Status:</strong> {{ vehicle.status }}</p>
          <hr />
        </div>
      </div>
      <div v-for="(comment, cIndex) in comments" :key="cIndex">
          <p v-if="comment.user.username === loggedInUser.username && comment.commentStatus === 'Approved'"><strong>Commented by: </strong></p>
          <p v-if="comment.user.username === loggedInUser.username && comment.commentStatus === 'Approved'"><strong>Username: </strong>{{comment.user.username}}</p>
          <p v-if="comment.user.username === loggedInUser.username && comment.commentStatus === 'Approved'"><strong>RentacarObject: </strong>{{comment.rentacarObject}}</p>
          <p v-if="comment.user.username === loggedInUser.username && comment.commentStatus === 'Approved'"><strong>Text: </strong>{{comment.text}}</p>
          <p v-if="comment.user.username === loggedInUser.username && comment.commentStatus === 'Approved'"><strong>Rating: </strong>{{comment.rating}}</p>
          <p v-if="comment.user.username === loggedInUser.username && comment.commentStatus === 'Approved'"><strong>Status: </strong>{{comment.commentStatus}}</p>
        </div>

      <div>
        <h3>Rentacar Objects</h3>
        <div v-for="(rentacar, rIndex) in order.rentacarObject" :key="rIndex">
          <p><strong>Rentacar Object:</strong> {{ rentacar }}</p>
          <hr />
        </div>
      </div>

      <hr />
    </div>
  </div>
  <div v-if="isManager">
    <router-link :to="'/rentacar/' + loggedInUser.rentalAgency">
      <h2>See your object {{loggedInUser.rentalAgency}}</h2>
    </router-link>
    <div v-for="(order, index) in searchedOrders" :key="index">
      <h2>Order {{ order.id }}</h2>
      <p><strong>Start Date:</strong> {{ order.startDate }}</p>
      <p><strong>Duration:</strong> {{ order.duration }} days</p>
      <p><strong>Total Price:</strong> {{ order.totalPrice }}</p>
      <p><strong>User:</strong> {{ order.user }}</p>
      <p><strong>Order status:</strong> {{ order.orderStatus }}</p>
      <button v-if="isManager && order.orderStatus === 'Pending'" v-on:click="confirmOrder(order)">Confirm</button>
      <button v-if="isManager && order.orderStatus === 'Pending' && !isCommented" v-on:click="commentRejection()">Reject</button>
      <input type="text" v-model="order.comment" v-if="isManager && order.orderStatus === 'Pending' && isCommented" placeholder="Reason for rejecting">
      <button v-if="isManager && order.orderStatus === 'Pending' && isCommented" v-on:click="rejectOrder(order)">Reject</button>
      <button v-if="isManager && order.orderStatus === 'InProgress'" v-on:click="returnOrder(order)">Return</button>

      <div>
        <h3>Vehicles</h3>
        <div v-for="(vehicle, vIndex) in order.vehicles" :key="vIndex">
          <p><strong>Brand:</strong> {{ vehicle.brand }}</p>
          <p><strong>Model:</strong> {{ vehicle.model }}</p>
          <p><strong>Price:</strong> {{ vehicle.totalPrice }}</p>
          <p><strong>Type:</strong> {{ vehicle.type }}</p>
          <p><strong>Status:</strong> {{ vehicle.status }}</p>
          <hr />
        </div>
      </div>

      <div>
        <h3>Rentacar Objects</h3>
        <div v-for="(rentacar, rIndex) in order.rentacarObject" :key="rIndex">
          <p><strong>Rentacar Object:</strong> {{ rentacar }}</p>
          <hr />
        </div>
      </div>
      <hr />
      </div>
      <div v-if="isAdmin || isManager">
        <div v-for="(comment, cIndex) in comments" :key="cIndex">
          <p><strong>Commented by: </strong></p>
          <p><strong>Username: </strong>{{comment.user.username}}</p>
          <p><strong>RentacarObject: </strong>{{comment.rentacarObject}}</p>
          <p><strong>Text: </strong>{{comment.text}}</p>
          <p><strong>Rating: </strong>{{comment.rating}}</p>
          <p><strong>Status: </strong>{{comment.commentStatus}}</p>
          <button v-if="comment.commentStatus === 'Pending' && isManager" v-on:click="approveComment(comment)">Approve comment</button>
          <button v-if="comment.commentStatus === 'Pending' && isManager" v-on:click="rejectComment(comment)">Reject comment</button>
        </div>
      </div>
</div>

</div>
        
			`,
  mounted() {
    let username = "";
    this.searchedOrders = this.orders;
    axios.get("rest/logedInUser").then((response) => {
      this.user = Object.assign({}, response.data);
      console.log("User", this.user);
      username = this.user.firstName + " " + this.user.lastName;
    });
    axios
      .get("rest/getUsers")
      .then((response) => {
        this.users = response.data;
      })
      .catch((error) => console.error(error));
    axios.get(`rest/getAllOrders/${username}`).then((response) => {
      this.orders = response.data;
      this.searchedOrders = this.orders;
    });
    axios.get('rest/getAllComments').then((response) => {
      this.comments = response.data;
    })
  },

  methods: {
    validation: function () {
      this.isValid = true;
      var user = {
        username: this.user.username,
        password: this.user.password,
        firstName: this.user.firstName,
        lastName: this.user.lastName,
        gender: this.user.gender,
        birthDate: this.user.birthDate,
        role: this.user.role,
        allRentals: this.user.allRentals,
        shoppingCart: this.user.shoppingCart,
        rewardPoints: this.user.rewardPoints,
        costumerType: this.user.costumerType,
        deleted: false,
      };

      axios
        .put("rest/update", { user })
        .then((response) =>
          toast(
            "User " +
              this.user.firstName +
              " " +
              this.user.lastName +
              " successfully updated"
          )
        )
        .catch((error) => console.error(error));
      this.$router.push("/");
    },
    searchByName() {
      this.searchedOrders = this.orders.filter((item) =>
        item.rentacarObject.some((obj) =>
          obj
            .toLowerCase()
            .includes(this.searchCriteria.rentacarObject.toLowerCase())
        )
      );
    },
    searchByPrice() {
      this.searchedOrders = this.orders.filter(
        (item) =>
          item.price >= this.searchCriteria.priceLow &&
          item.price <= this.searchCriteria.priceUp
      );
    },
    searchByDate() {
      this.searchedOrders = this.orders.filter((item) => {
        startDateTimestamp = new Date(this.item.startDate).getTime();
        endDateTimestamp =
          new Date(this.item.startDate).getTime() +
          item.duration * (24 * 60 * 60 * 1000);
        startDateTimestamp >= this.searchCriteria.startDate &&
          endDateTimestamp >= this.searchCriteria.endDate;
      });
    },
    sortBy(property) {
      this.sortCriteria[property] =
        this.sortCriteria[property] === "asc" ? "desc" : "asc";

      this.searchedOrders.sort((a, b) => {
        const order = this.sortCriteria[property] === "asc" ? 1 : -1;

        if (a[property] < b[property]) return -1 * order;
        if (a[property] > b[property]) return 1 * order;
        return 0;
      });
    },
    cancelOrder(order) {
      const updatedOrder = order;
      updatedOrder.orderStatus = OrderStatus.Canceled;
      console.log(updatedOrder);
      axios
        .put("rest/updateOrder", { updatedOrder })
        .then(toast("Order updated"));
      this.user.rewardPoints -= (order.totalPrice / 1000) * 133 * 4;
      const user = this.user;
      axios.put("rest/update", { user }).then(toast("User updated"));
    },
    confirmOrder(order) {
      const updatedOrder = order;
      updatedOrder.orderStatus = OrderStatus.Confirmed;
      console.log(updatedOrder);
      axios
        .put("rest/updateOrder", { updatedOrder })
        .then(toast("Order updated"));
      const currentDate = new Date();
      const formattedDate = currentDate.toISOString().split("T")[0];
      for (o of this.searchedOrders) {
        if (o.id == order.id) {
          if (o.startDate === formattedDate) {
            o.orderStatus = OrderStatus.InProgress;
            for (v of o.vehicles) {
              v.status = Status.Rented;
              axios
                .put("rest/updateVehicle", { v })
                .then(toast("Vehicle updated"));
            }
            const updatedOrder = o;
            axios
              .put("rest/updateOrder", { updatedOrder })
              .then(toast("Order updated"));
          }
        }
      }
    },
    rejectOrder(order) {
      const updatedOrder = order;
      updatedOrder.orderStatus = OrderStatus.Rejected;
      console.log(updatedOrder);
      axios
        .put("rest/updateOrder", { updatedOrder })
        .then(toast("Order updated"));
    },
    returnOrder(order) {
      const updatedOrder = order;
      updatedOrder.orderStatus = OrderStatus.Returned;
      for (o of this.searchedOrders) {
        if (o.id == order.id) {
          for (v of o.vehicles) {
            v.status = Status.Available;
            axios
              .put("rest/updateVehicle", { v })
              .then(toast("Vehicle updated"));
          }
          const updatedOrder = o;
          axios
            .put("rest/updateOrder", { updatedOrder })
            .then(toast("Order updated"));
        }
      }
      axios
        .put("rest/updateOrder", { updatedOrder })
        .then(toast("Order updated"));
    },
    commentRejection() {
      this.isCommented = true;
    },
    commentRental(){
      this.commentRent = true;
    },
    commentIt(rentacarObject){
      const comment = this.comment
      comment.user = this.user
      comment.rentacarObject = rentacarObject 
      axios.post('rest/createComment', {comment}).then(toast("Comment created"))
    },
    approveComment(comment){
      comment.commentStatus = commentStatus.Approved
      axios.put('rest/updateComment', {comment}).then(toast("Comment approved"))
    },
    rejectComment(comment){
      comment.commentStatus = commentStatus.Rejected
      axios.put('rest/updateComment', {comment}).then(toast("Comment rejected "))
    },
  },
});
