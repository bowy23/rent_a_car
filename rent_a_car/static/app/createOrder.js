const OrderStatus = {
  Pending: "Pending",
  Confirmed: "Confirmed",
  InProgress: "InProgress",
  Returned: "Returned",
  Rejected: "Rejected",
  Canceled: "Canceled",
};

Vue.component("createOrder", {
  data: function () {
    return {
      order: {
        id: "",
        vehicles: "",
        rentacarObject: "",
        startDate: "",
        endDate: "",
        duration: "",
        price: "",
        user: "",
        status: "",
      },
      vehicles: [],
      orders: [],
      cart: [],
      showCart: false,
    };
  },
  computed: {
    filteredVehicles() {
      return this.vehicles.filter((item) => {
        return item.startDate !== "" && item.startDate >= this.order.startDate;
      });
    },
    totalPrice() {
      return this.cart.reduce((total, item) => total + item.price, 0);
    },
  },

  template: `
      <div>
        <form>
            <input type="date" name="startdate" v-model="order.startDate">
            <input type="date" name="enddate" v-model="order.endDate">
            <button v-on:click.prevent="makeShowCart(showCart)">Show cart</button>
            <div v-if="!showCart">
            <p><strong>Vehicles</strong></p>
            <ul>
                <li v-for="(item, index) in vehicles" :key="index">
                    <p><strong>Type:</strong> {{ item.type }}</p>
                    <p><strong>Fuel:</strong> {{ item.fuel }}</p>
                    <p><strong>Brand:</strong> {{ item.brand }}</p>
                    <p><strong>Model:</strong> {{ item.model }}</p>
                    <p><strong>Type:</strong> {{ item.type }}</p>
                    <p><strong>Transmission:</strong> {{ item.transmission }}</p>
                    <p><strong>Price:</strong> {{ item.price }}</p>
                    <p><strong>Consumption:</strong> {{ item.consumption }}</p>
                    <p><strong>Doors:</strong> {{ item.doors }}</p>
                    <p><strong>Passenger:</strong> {{ item.passengers }}</p>
                    <p><strong>Description:</strong> {{ item.description }}</p>
                    <p><strong>Picture:</strong> {{ item.picture }}</p>
                    <p><strong>Status:</strong> {{ item.status }}</p>
                    <button v-on:click.prevent="addToCart(item)">Add to cart</button>
                    </li>
            </ul>   
        </div>
        <div v-if="showCart">
            <ul>
                <li v-for="(item, index) in cart" :key="index">
                <p><strong>Brand:</strong> {{ item.brand }}</p>
                <p><strong>Model:</strong> {{ item.model }}</p>
                <p><strong>Price:</strong> {{ item.price }}</p>
                <p><strong>Picture:</strong> {{ item.picture }}</p>
                <button v-on:click="removeFromCart(item)">Remove from cart</button>
                </li>
                <p><strong>Total:</strong> {{ totalPrice }}</p>
                <button v-if="totalPrice !== 0" v-on:click="createOrder()">Create order</button>
            </ul>
        </div>
        </form>
      </div>
      `,
  methods: {
    getAvailableVehiclesForDate(date) {
      axios.get(`rest/getAvailableForDate`, { date }).then((response) => {
        this.vehicles = response.data;
      });
    },
    addToCart(vehicle) {
      this.cart.push(vehicle);
      alert("Vehicle " + vehicle.brand + " " + vehicle.model + "Added to cart");
    },
    makeShowCart(showCart) {
      this.showCart = !showCart;
    },
    removeFromCart(vehicle) {
      this.cart = this.cart.filter((item) => item.brand !== vehicle.brand);
    },
    createOrder() {
      const randomPart = Math.floor(1000 + Math.random() * 9000); // Ensures a 4-digit number
      var orderId = `${Date.now()}-${randomPart}`;
      orderId = orderId.substring(0, 10);
      const startDateTimestamp = new Date(this.order.startDate).getTime();
      const endDateTimestamp = new Date(this.order.endDate).getTime();

      const duration = (endDateTimestamp - startDateTimestamp) / (24 * 60 * 60 * 1000);
      const uniqueRentacarObjects = new Set();

    this.cart.forEach(vehicle => {
        uniqueRentacarObjects.add(vehicle.rentACarObject);
    });

    const rentacarObject = [...uniqueRentacarObjects];
      var order = {
        id: orderId,
        vehicles: this.cart,
        rentacarObject: rentacarObject,
        startDate: this.order.startDate,
        duration: duration,
        totalPrice: this.totalPrice,
        user: this.order.user.firstName + " " + this.order.user.lastName,
        orderStatus: OrderStatus.Pending,
      };
      axios.post("rest/createOrder", { order }).then((response) => {
        toast("Order created");
      });
      this.order.user.rewardPoints += this.totalPrice/1000 * 133
      const user = this.order.user
      console.log(user);
      axios.put("rest/update", {user}).then(toast("User updated"))
    },
  },
  mounted() {
    axios.get("rest/getAllOrders").then((response) => {
      this.orders = response.data;
    });

    axios.get("rest/getAvailableVehicles").then((response) => {
      this.vehicles = response.data;
    });

    axios
      .get("rest/logedInUser")
      .then((response) => {
        this.order.user = Object.assign({}, response.data);
      })
      .catch((error) => {
        console.error("Error fetching logedInUser:", error);
      });
  },
  watch: {
    // Watch for changes in the selected start date and trigger fetching of available vehicles
    "order.startDate": "getAvailableVehiclesForDate",
  },
});
