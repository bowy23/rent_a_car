Vue.component("login", {
  data: function () {
    return {
      username: "",
      password: "",
    };
  },

  template: `
    <div>
    <form v-on:submit="login()">
        <input type="text" v-model="username" placeholder="Username" name="username"><br>
        <input type="password" v-model="password" placeholder="Password" name="password"><br>
        <input type="submit" value="Log in" name="logIn"><br>
    </form>
</div>
    `,
  methods: {
    login() {
      if (this.username !== "" && this.password !== "") {
        axios
          .post("rest/login", {
            username: this.username,
            password: this.password,
          })
          .then((response) => {
            toast("User " + this.username + " successfully logged in");
          })
          .catch((error) => console.error(error));
        this.$router.push("/");
      }
    },
  },
});
