const registration = { template: "<registration></registration>"}
const home = { template: "<home></home>"}
const login = { template: "<login></login>"}
const manager = { template: "<manager></manager>"}
const rentacar = { template: "<rentacar></rentacar>"}
const profile = { template: "<profile></profile>"}
const createRentacar = { template: "<createRentacar></createRentacar>"}
const editVehicles = { template: "<editVehicles></editVehicles>"}
const createOrder = { template: "<createOrder></createOrder>"}

const router = new VueRouter({
	  mode: 'hash',
	  routes: [
		{path: '/', component: home},
		{path: '/registration', component: registration},
		{path: '/login', component: login},
		{path: '/manager', component: manager},
		{path: '/rentacar/:name', component: rentacar},
		{path: '/profile', component: profile},
		{path: '/createRentacar', component: createRentacar},
		{path: '/editVehicles/:name', component: editVehicles},
		{path: '/createOrder', component: createOrder},
	  ]
});

var app = new Vue({
	router,
	el: '#rentacar'
});