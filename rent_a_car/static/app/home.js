Vue.component("home", {
  data: function () {
    return {
      logedInUser: {},
      rentACars: [],
      searchCriteria: {
        name: "",
        location: "",
        rating: "",
        vehicleType: "",
      },
      sortCriteria: {
        name: "",
        location: "",
        rating: "",
      },
      filterCriteria: {
        type: "",
        fuel: "",
        status: "open",
      },
      isFilterActive: true,
    };
  },
  template: `
        <div>
            <button v-if="Object.keys(logedInUser).length === 0" v-on:click="logIn()">Log in</button>
            <button v-on:click="signIn()">Sign in</button>
            <button v-if="logedInUser.role === 'Admin'" v-on:click="createManager()">Create manager</button>
            <button v-if="logedInUser.role === 'Admin'" v-on:click="createObject()">Create rentacar object</button>
            <button v-if="logedInUser.role === 'Manager'" v-on:click="editVehicles()">Edit vehicles</button>
            <button v-if="Object.keys(logedInUser).length > 0" v-on:click="logOut()">Log out</button>
            <button v-if="Object.keys(logedInUser).length > 0" v-on:click="profile()">Profile</button><br><br>
            <button v-if="logedInUser.role === 'Costumer'" v-on:click="createOrder()">Create order</button>
            <div>
                <input type="text" v-model="searchCriteria.name" id="searchName" placeholder="search name">
                <button v-on:click="searchByName()">Search by Name</button>
                <input type="text" v-model="searchCriteria.location" id="searchLocation" placeholder="search location">
                <button v-on:click="searchByLocation()">Search by Location</button>
                <input type="text" v-model="searchCriteria.rating" id="searchRating" placeholder="search rating">
                <button v-on:click="searchByRating()">Search by Rating</button>
                <input type="text" v-model="searchCriteria.vehicleType" id="searchVehicleType" placeholder="search vehicle type">
                <button v-on:click="searchByVehicleType()">Search by Vehicle Type</button>
            </div>
            <div>
                <select name="type" id="type" v-model="filterCriteria.type">
                    <option value="">No Filter</option>
                    <option value="car">Car</option>
                    <option value="van">Van</option>
                    <option value="bike">Bike</option>
                    <option value="quad">Quad</option>
                    <option value="mobilehome">Mobilehome</option>
                </select>
                <select name="fuel" id="fuel" v-model="filterCriteria.fuel">
                    <option value="">No Filter</option>
                    <option value="diesel">Diesel</option>
                    <option value="petrol">Petrol</option>
                    <option value="hybrid">Hybrid</option>
                    <option value="electric">Electric</option>
                </select>
                <select name="status" id="status" v-model="filterCriteria.status">
                    <option value="">No Filter</option>
                    <option value="open">Open</option>
                </select>
                <button v-on:click="resetFilter()">Reset Filter</button>
            </div>
            <div>
                <button v-on:click="sortBy('name')">Sort by Name</button>
                <button v-on:click="sortBy('location')">Sort by Location</button>
                <button v-on:click="sortBy('rating')">Sort by Rating</button>
            </div>
            <div v-for="(item, index) in filteredRentACars" :key="index" class="rentacar-info">
                <router-link :to="'/rentacar/' + item.name">
                    <h2>{{ item ? item.name : 'Name not available' }}</h2>
                    <p><strong>Location:</strong> {{ item ? item.location : 'Location not available' }}</p>
                    <p><strong>Status:</strong> {{ item ? item.status : 'Status not available' }}</p>
                    <p><strong>Logo:</strong> {{ item ? item.logo : 'Logo not available' }}</p>
                    <p><strong>Rating:</strong> {{ item ? item.rating : 'Rating not available' }}</p>
                </router-link>
            </div>
            <div v-if="logedInUser.role === 'Manager'">
                <h3>Rentacars for {{ logedInUser.rentalAgency }}</h3>
                <div v-for="(item, index) in managerRentACars" :key="index" class="rentacar-info">
                    <router-link :to="'/rentacar/' + item.name">
                    <h2>{{ item ? item.name : 'Name not available' }}</h2>
                    <p><strong>Location:</strong> {{ item ? item.location : 'Location not available' }}</p>
                    <p><strong>Status:</strong> {{ item ? item.status : 'Status not available' }}</p>
                    <p><strong>Logo:</strong> {{ item ? item.logo : 'Logo not available' }}</p>
                    <p><strong>Rating:</strong> {{ item ? item.rating : 'Rating not available' }}</p>
                    </router-link>
                </div>
            </div>  
        </div>
    `,
  computed: {
    isUserAdmin() {
      return this.logedInUser.role !== undefined;
    },
    managerRentACars() {
      return this.rentACars.filter(
        (item) => item.name === this.logedInUser.rentalAgency
      );
    },
    searchedRentACars() {
      return this.rentACars.filter((item) => {
        const nameMatch = item.name
          .toLowerCase()
          .includes(this.searchCriteria.name.toLowerCase());
        const locationMatch = item.location
          .toLowerCase()
          .includes(this.searchCriteria.location.toLowerCase());
        const ratingMatch = item.rating
          .toString()
          .includes(this.searchCriteria.rating);
        const vehicleTypeMatch = item.vehicles.some((vehicle) =>
          vehicle.type
            .toLowerCase()
            .includes(this.searchCriteria.vehicleType.toLowerCase())
        );

        return nameMatch && locationMatch && ratingMatch && vehicleTypeMatch;
      });
    },
    sortedRentACars() {
      return this.searchedRentACars.slice().sort((a, b) => {
        if (this.sortCriteria.name) {
          const order = this.sortCriteria.name === "asc" ? 1 : -1;
          return (a.name.toLowerCase() < b.name.toLowerCase() ? -1 : 1) * order;
        } else if (this.sortCriteria.location) {
          const order = this.sortCriteria.location === "asc" ? 1 : -1;
          return (
            (a.location.toLowerCase() < b.location.toLowerCase() ? -1 : 1) *
            order
          );
        } else if (this.sortCriteria.rating) {
          const order = this.sortCriteria.rating === "asc" ? 1 : -1;
          return (a.rating - b.rating) * order;
        } else {
          return 0;
        }
      });
    },
    filteredRentACars() {
      if (this.isFilterActive) {
        return this.sortedRentACars.filter((item) => {
          const vehicleTypeMatch = this.filterCriteria.type
            ? item.vehicles.some((vehicle) =>
                vehicle.type.toLowerCase().includes(this.filterCriteria.type)
              )
            : true;
          const vehicleFuelMatch = this.filterCriteria.fuel
            ? item.vehicles.some((vehicle) =>
                vehicle.fuel.toLowerCase().includes(this.filterCriteria.fuel)
              )
            : true;
          const statusMatch = item.status
            .toLowerCase()
            .includes(this.filterCriteria.status);

          return vehicleTypeMatch && vehicleFuelMatch && statusMatch;
        });
      } else {
        return this.sortedRentACars;
      }
    },
  },
  methods: {
    signIn() {
      this.$router.push("/registration");
    },
    logIn() {
      this.$router.push("/login");
    },
    logOut() {
      this.logedInUser = {};
      axios
        .get("rest/logout")
        .then((response) => toast("User successfully logedout"));
      this.$router.push("/");
    },
    createManager() {
      this.$router.push("/manager");
    },
    createObject() {
      this.$router.push("/createRentacar");
    },
    profile() {
      this.$router.push("/profile");
    },
    searchByName() {
      this.searchedRentACars = this.rentACars.filter((item) =>
        item.name.toLowerCase().includes(this.searchCriteria.name.toLowerCase())
      );
    },

    searchByLocation() {
      this.searchedRentACars = this.rentACars.filter((item) =>
        item.location
          .toLowerCase()
          .includes(this.searchCriteria.location.toLowerCase())
      );
    },

    searchByRating() {
      this.searchedRentACars = this.rentACars.filter((item) =>
        item.rating.toString().includes(this.searchCriteria.rating)
      );
    },

    searchByVehicleType() {
      this.searchedRentACars = this.rentACars.filter((item) =>
        item.vehicles.some((vehicle) =>
          vehicle.type
            .toLowerCase()
            .includes(this.searchCriteria.vehicleType.toLowerCase())
        )
      );
    },
    sortBy(property) {
      this.sortCriteria[property] =
        this.sortCriteria[property] === "asc" ? "desc" : "asc";

      // Sort the searchedRentACars array based on the chosen property and order
      this.searchedRentACars.sort((a, b) => {
        const order = this.sortCriteria[property] === "asc" ? 1 : -1;

        if (a[property] < b[property]) return -1 * order;
        if (a[property] > b[property]) return 1 * order;
        return 0;
      });
    },
    toggleFilterStatus() {
      this.isFilterActive = !this.isFilterActive;
    },
    resetFilter() {
      this.filterCriteria = {
        type: "",
        fuel: "",
        status: "",
      };
      this.isFilterActive = true;
    },
    editVehicles() {
      const name = this.logedInUser.rentalAgency;
      this.$router.push(`/editVehicles/${name}`);
    },
    createOrder() {
      this.$router.push("/createOrder");
    },
  },
  mounted() {
    axios
      .get("rest/logedInUser")
      .then((response) => {
        this.logedInUser = Object.assign({}, response.data);
      })
      .catch((error) => {
        console.error("Error fetching logedInUser:", error);
      });

    axios
      .get("rest/getRentacars")
      .then((response) => {
        this.rentACars = response.data;
      })
      .catch((error) => {
        console.error("Error fetching rentacars:", error);
      });
  },
});
