Vue.component("rentacar", {
  data: function () {
    return {
      rentacar: {},
      user: {},
      orders: [],
      comments: [],
    };
  },
  computed: {
    loggedInUser() {
      return this.user;
    },
  },

  template: `
    <div>
        <p><strong>Name</strong> {{rentacar.name}}</p>
        <p><strong>Working Hours</strong>  {{rentacar.workingHours}}</p>
        <p><strong>Status</strong>  {{rentacar.status}}</p>
        <p><strong>Location</strong>  {{rentacar.location}}</p>
        <p><strong>Logo</strong>  {{rentacar.logo}}</p>
        <p><strong>Rating</strong>  {{rentacar.rating}}</p>
        <p><strong>Comments</strong></p>
        <div v-for="(comment, cIndex) in comments" :key="'comment-' + cIndex">
          <p><strong>Commented by: </strong></p>
          <p><strong>Username: </strong>{{comment.user.username}}</p>
          <p><strong>RentacarObject: </strong>{{comment.rentacarObject}}</p>
          <p><strong>Text: </strong>{{comment.text}}</p>
          <p><strong>Rating: </strong>{{comment.rating}}</p>
          </div>
          <div v-if="loggedInUser.role === 'Manager'" v-for="(order1, oIndex1) in orders" :key="'order1-' + oIndex1">
              <p><strong>Users that ordered:</strong> {{order1.user}}</p>
          </div>
        <p v-if="loggedInUser.role === 'Manager'"><strong>Orders</strong></p>
        <div v-if="loggedInUser.role === 'Manager'" v-for="(order, oIndex) in orders" :key="'order-' + oIndex">
            <h2>Order {{ order.id }}</h2>
            <p><strong>Start Date:</strong> {{ order.startDate }}</p>
            <p><strong>Duration:</strong> {{ order.duration }} days</p>
            <p><strong>Total Price:</strong> {{ order.totalPrice }}</p>
            <p><strong>User:</strong> {{ order.user }}</p>
            <p><strong>Order status:</strong> {{ order.orderStatus }}</p>
        </div>
        <p><strong>Vehicles</strong></p>
        <ul>
            <li v-for="(vehicle, vindex) in rentacar.vehicles" :key="'vehicle-' + vindex">
                <p><strong>Type:</strong> {{ vehicle.type }}</p>
                <p><strong>Fuel:</strong> {{ vehicle.fuel }}</p>
                <p><strong>Brand:</strong> {{ vehicle.brand }}</p>
                <p><strong>Model:</strong> {{ vehicle.model }}</p>
                <p><strong>Type:</strong> {{ vehicle.type }}</p>
                <p><strong>Transmission:</strong> {{ vehicle.transmission }}</p>
                <p><strong>Price:</strong> {{ vehicle.price }}</p>
                <p><strong>Consumption:</strong> {{ vehicle.consumption }}</p>
                <p><strong>Doors:</strong> {{ vehicle.doors }}</p>
                <p><strong>Passenger:</strong> {{ vehicle.passengers }}</p>
                <p><strong>Description:</strong> {{ vehicle.description }}</p>
                <p><strong>Picture:</strong> {{ vehicle.picture }}</p>
                <p><strong>Status:</strong> {{ vehicle.status }}</p>
            </li>
        </ul>
    </div>
    `,
  mounted() {
    const name = this.$route.params.name;
    this.fetchRentacarDetails(name);
    axios.get("rest/logedInUser").then((response) => {
      this.user = response.data;
    });

    axios.get(`rest/getOrdersByObject/${name}`).then((response) => {
      this.orders = response.data;
    });

    axios.get(`rest/getCommentsForObject/${name}`).then((response) => {
      this.comments = response.data;
    });
  },
  methods: {
    fetchRentacarDetails(name) {
      axios
        .get(`rest/getRentacar/${name}`)
        .then((response) => {
          this.rentacar = response.data;
        })
        .catch((error) => {
          console.error("Error fetching rentacar details:", error);
        });
    },
  },
});
