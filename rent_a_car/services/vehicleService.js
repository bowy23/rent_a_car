const fs = require("fs");

let vehicles = [];

async function addVehicle(vehicle) {
  return new Promise((resolve, reject) => {
    const filePath = "data/vehicles.json";
    fs.readFile(filePath, "utf8", (err, data) => {
      if (err) {
        console.error("Error reading file:", err);
        reject(err);
        return;
      }

      let vehicles = [];

      try {
        vehicles = JSON.parse(data);

        if (!Array.isArray(vehicles)) {
          console.error("Existing data is not an array:", vehicles);
          reject("Existing data is not an array");
          return;
        }
      } catch (parseError) {
        console.error("Error parsing JSON:", parseError);
        reject(parseError);
        return;
      }

      vehicles.push(vehicle.Vehicle);

      fs.writeFile(filePath, JSON.stringify(vehicles), (writeErr) => {
        if (writeErr) {
          console.error("Error writing to file:", writeErr);
          reject(writeErr);
        } else {
          console.log("Vehicle added successfully!");
          resolve(vehicles);
        }
      });
    });
  });
}

async function deleteVehicle(brand, model) {
  return new Promise((resolve, reject) => {
    const filePath = "data/vehicles.json";
    fs.readFile(filePath, "utf8", (err, data) => {
      if (err) {
        console.error("Error reading file:", err);
        reject(err);
        return;
      }

      let vehicles = [];

      try {
        vehicles = JSON.parse(data);

        if (!Array.isArray(vehicles)) {
          console.error("Existing data is not an array:", vehicles);
          reject("Existing data is not an array");
          return;
        }
      } catch (parseError) {
        console.error("Error parsing JSON:", parseError);
        reject(parseError);
        return;
      }

      const index = vehicles.findIndex(
        (vehicle) => vehicle.brand === brand && vehicle.model === model
      );

      if (index !== -1) {
        vehicles[index].deleted = true;

        fs.writeFile(filePath, JSON.stringify(vehicles), (writeErr) => {
          if (writeErr) {
            console.error("Error writing to file:", writeErr);
            reject(writeErr);
          } else {
            console.log("Vehicle added successfully!");
            resolve(vehicles);
          }
        });
      }
    });
  });
}
async function updateVehicle(vehicle) {
  return new Promise((resolve, reject) => {
    const filePath = "data/vehicles.json";
    fs.readFile(filePath, "utf8", (err, data) => {
      if (err) {
        console.error("Error reading file:", err);
        reject(err);
        return;
      }

      let vehicles = [];

      try {
        vehicles = JSON.parse(data);

        if (!Array.isArray(vehicles)) {
          console.error("Existing data is not an array:", vehicles);
          reject("Existing data is not an array");
          return;
        }
      } catch (parseError) {
        console.error("Error parsing JSON:", parseError);
        reject(parseError);
        return;
      }

      const index = vehicles.findIndex(
        (veh) => veh.brand === vehicle.brand && veh.model === vehicle.model
      );

      if (index !== -1) {
        vehicles[index] = vehicle;

        fs.writeFile(filePath, JSON.stringify(vehicles), (writeErr) => {
          if (writeErr) {
            console.error("Error writing to file:", writeErr);
            reject(writeErr);
          } else {
            console.log("Vehicle added successfully!");
            resolve(vehicles);
          }
        });
      }
    });
  });
}

async function getAvailableVehicles() {
  return new Promise((resolve, reject) => {
    const filePath = "data/vehicles.json";
    fs.readFile(filePath, "utf8", (err, data) => {
      if (err) {
        console.error("Error reading file:", err);
        reject(err);
        return;
      }

      let vehiclesData = [];

      try {
        vehiclesData = JSON.parse(data);

        if (!Array.isArray(vehiclesData)) {
          console.error("Existing data is not an array:", vehiclesData);
          reject("Existing data is not an array");
          return;
        }
      } catch (parseError) {
        console.error("Error parsing JSON:", parseError);
        reject(parseError);
        return;
      }

      vehicles = vehiclesData.filter((v) => v.status === "available" && v.deleted === false);
      resolve(vehicles);
    });
  });
}

module.exports = {
  addVehicle,
  deleteVehicle,
  updateVehicle,
  getAvailableVehicles,
};
