const fs = require("fs");

class RentACar{
    constructor(name, vehicles, workingHours, status, location, logo, rating){
        this.name= name;
        this.vehicles = vehicles || [];
        this.workingHours = workingHours;
        this.status = status; 
        this.location = location;
        this.logo = logo;
        this.rating = rating;
    }
}

class RentACars{
    constructor(){
        this.values = [];
        this.map = {};
        var self = this;
        fs.readFile("rentACars.json", 'utf-8', function(err, data){
            if(err){
                console.error(err);
                return;
            }
            try {
                const rentACars = JSON.parse(data);
                for(rentACarData of rentACars){
                    let rentACar = new RentACar(
                        rentACarData.name,
                        rentACarData.vehicles,
                        rentACarData.workingHours,
                        rentACarData.status,
                        rentACarData.location,
                        rentACarData.logo,
                        rentACarData.rating
                    );
                    this.values.push(rentACar);
                    this.map[rentACar.address] = rentACar;
                }

            } catch (parseError) {
                console.error(parseError);
                callback(parseError, null);
            }
        })
    }
}

module.exports = {RentACar, RentACars}