const fs = require("fs");
class Vehicle{
    constructor(brand, model, price, type, rentACarObject, transmission, fuel, consumption, doors, passengers, description, picture, status){
        this.brand = brand;
        this.model = model;
        this.price = price;
        this.type = type;
        this.rentACarObject = rentACarObject;
        this.transmission = transmission;
        this.fuel = fuel;
        this.consumption = consumption;
        this.doors = doors;
        this.passengers = passengers;
        this.description = description;
        this.picture = picture;
        this.status = status;
    }
}

class Vehicles{
    constructor(){
        this.values = [];
        this.map = {};
        var self = this;
        fs.readFile("vehicles.json", 'utf-8', function(err, data){
            if(err){
                console.error(err);
                return;
            }
            try {
                const vehicles = JSON.parse(data);
                for(vehicleData of vehicles){
                    let vehicle = new Vehicle(
                        vehicleData.brand,
                        vehicleData.model,
                        vehicleData.price,
                        vehicleData.type,
                        vehicleData.rentACarObject,
                        vehicleData.transmission,
                        vehicleData.fuel,
                        vehicleData.consumption,
                        vehicleData.doors,
                        vehicleData.passengers,
                        vehicleData.description,
                        vehicleData.image,
                        "available"
                    );
                    this.values.push(vehicle);
                    this.map[vehicle.image] = vehicle;
                }

            } catch (parseError) {
                console.error(parseError);
                callback(parseError, null);
            }
        })
    }
}

module.exports = {Vehicle, Vehicles}