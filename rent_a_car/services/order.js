const fs = require("fs");

class Order{
    constructor(id, vehicles, rentacarObject, startDate, duration, price, user, status){
        this.id = id;
        this.vehicles = vehicles;
        this.rentacarObject = rentacarObject;
        this.startDate = startDate;
        this.duration = duration;
        this.price = price;
        this.user = user;
        this.status = status;
    }
}

class Orders{
    constructor(){
        this.values = [];
        this.map = {};
        var self = null;
        fs.readFile("orders.json", 'utf-8', function(err, data){
            if(err){
                console.error(err);
                return;
            }
            try{
                const orders = JSON.parse(data);
                for(const orderData of orders){
                    let order = new Order(
                        orderData.id,
                        orderData.vehicles,
                        orderData.rentacarObject,
                        orderData.startDate,
                        orderData.duration,
                        orderData.price,
                        orderData.user,
                        'Pending'
                    );
                    this.values.push(order);
                    this.map[order.vehicles] = order;
                }
            } catch(parseError){
                console.error(parseError);
                callback(parseError, null);
            }
        });
    }
}

module.exports = {Order, Orders};