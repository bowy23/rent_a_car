const fs = require("fs");

let orders = [];

async function getallOrders() {
  return new Promise((resolve, reject) => {
    const filePath = "data/orders.json";

    fs.readFile(filePath, "utf8", (err, data) => {
      if (err) {
        console.error("Error reading file:", err);
        reject(err);
        return;
      }

      let ordersArray = [];

      try {
        // Parse the existing content as JSON
        ordersArray = JSON.parse(data);

        // Ensure that ordersArray is an array
        if (!Array.isArray(ordersArray)) {
          console.error("Existing data is not an array:", ordersArray);
          reject("Existing data is not an array");
          return;
        }
      } catch (parseError) {
        console.error("Error parsing JSON:", parseError);
        reject(parseError);
        return;
      }

      orders = ordersArray;
      resolve(orders)
    });
  });
}

async function getOrders() {
  orders = await getallOrders();
  return orders;
}

async function createOrder(order){
    return new Promise((resolve, reject) => {
        const filePath = "data/orders.json";
        fs.readFile(filePath, "utf8", (err, data) => {
          if (err) {
            console.error("Error reading file:", err);
            reject(err);
            return;
          }
    
          let orders = [];
    
          try {
            orders = JSON.parse(data);
    
            if (!Array.isArray(orders)) {
              console.error("Existing data is not an array:", orders);
              reject("Existing data is not an array");
              return;
            }
          } catch (parseError) {
            console.error("Error parsing JSON:", parseError);
            reject(parseError);
            return;
          }
    
          orders.push(order.order);
    
          fs.writeFile(filePath, JSON.stringify(orders), (writeErr) => {
            if (writeErr) {
              console.error("Error writing to file:", writeErr);
              reject(writeErr);
            } else {
              console.log("Vehicle added successfully!");
              resolve(orders);
            }
          });
        });
      });
}

function updateOrder(order) {
  const filePath = "data/orders.json";

  fs.readFile(filePath, 'utf8', (err, data) => {
      if (err) {
          console.error("Error reading file:", err);
          return;
      }

      let ordersArray = [];

      try {
          ordersArray = JSON.parse(data);

          if (!Array.isArray(ordersArray)) {
              console.error("Existing data is not an array:", ordersArray);
              return;
          }
          const orderIndex = ordersArray.findIndex(o => o.id === order.id)

          if (orderIndex !== -1) {
              ordersArray[orderIndex] = order;

              fs.writeFile(filePath, JSON.stringify(ordersArray, null, 2), (writeErr) => {
                  if (writeErr) {
                      console.error("Error writing to file:", writeErr);
                  } else {
                      console.log("Order updated successfully!");
                  }
              });
          } else {
              console.error("Order not found for update.");
          }
      } catch (parseError) {
          console.error("Error parsing JSON:", parseError);
      }
  });
}

async function getOrdersByObject(rentacarObject) {
  return new Promise((resolve, reject) => {
    const filePath = "data/orders.json";

    fs.readFile(filePath, "utf8", (err, data) => {
      if (err) {
        console.error("Error reading file:", err);
        reject(err);
        return;
      }

      let ordersArray = [];

      try {
        // Parse the existing content as JSON
        ordersArray = JSON.parse(data);

        // Ensure that ordersArray is an array
        if (!Array.isArray(ordersArray)) {
          console.error("Existing data is not an array:", ordersArray);
          reject("Existing data is not an array");
          return;
        }
      } catch (parseError) {
        console.error("Error parsing JSON:", parseError);
        reject(parseError);
        return;
      }

      orders = ordersArray.filter(c => c.rentacarObject.some(r => r === rentacarObject) );
      resolve(orders)
    });
  });
}

module.exports = {getOrders, createOrder, getOrdersByObject, updateOrder};
