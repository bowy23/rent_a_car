const fs = require("fs");

class Location{
    constructor(latitude, longitude, address){
        this.latitude = latitude;
        this.longitude = longitude;
        this.address = address;
    }
}

class Locations{
    constructor(){
        this.values = [];
        this.map = {};
        var self = null;
        fs.readFile("locations.json", 'utf-8', function(err, data){
            if(err){
                console.error(err);
                return;
            }
            try{
                const locations = JSON.parse(data);
                for(const locationData of locations){
                    let location = new Location(
                        locationData.name,
                        locationData.discount,
                        locationData.requiredPoints
                    );
                    this.values.push(location);
                    this.map[location.address] = location;
                }
            } catch(parseError){
                console.error(parseError);
                callback(parseError, null);
            }
        });
    }
}

module.exports = {Location, Locations};