const fs = require("fs");

class Comment{
    constructor(user, rentACar, text, rating){
        this.user = user;
        this.rentACar = rentACar;
        this.text = text;
        this.rating = rating || 0;
    }
}

class Comments{
    constructor(){
        this.values = [];
        this.map = {};
        var self = null;
        fs.readFile("comments.json", 'utf-8', function(err, data){
            if(err){
                console.error(err);
                return;
            }
            try{
                const comments = JSON.parse(data);
                for(const commentData of comments){
                    let comment = new Comment(
                        commentData.user,
                        commentData.rentACar,
                        commentData.text,
                        commentData.rating
                    );
                    this.values.push(comment);
                    this.map[comment.vehicles] = comment;
                }
            } catch(parseError){
                console.error(parseError);
                callback(parseError, null);
            }
        });
    }
}

module.exports = {Comment, Comments};