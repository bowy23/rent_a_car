const fs = require('fs')

const Role = {
    Costumer: 'Costumer',
    Manager: 'Manager',
    Admin: 'Admin'
}

class User {
    constructor(){
        this.username = "";
        this.password = "";
        this.firstName = "";
        this.lastName = "";
        this.gender = "";
        this.birthDate = "";
        this.role = "";
        this.deleted = "";
    }
}

class Costumer {
    constructor(username, password, firstName, lastName, gender, birthDate, role, allRentals){
        this.username = username;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.birthDate = new Date(birthDate);
        this.role = role;
        this.allRentals = allRentals;
        this.deleted = false;
        this.shoppingCart = [];
        this.rewardPoints = 0;
        this.costumerType = null;
    }
}

class Admin {
    constructor(username, password, firstName, lastName, gender, birthDate, role){
        this.username = username;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.birthDate = new Date(birthDate);
        this.role = role;
        this.deleted = false;
    }
}

class Manager {
    constructor(username, password, firstName, lastName, gender, birthDate, role, rentalAgency){
        this.username = username;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.birthDate = new Date(birthDate);
        this.role = role;
        this.rentalAgency = rentalAgency;
        this.deleted = false;   
    }
}

class Users{
    constructor(){
        this.values = [];
        this.map = {};
        var self = this;
        fs.readFile("data/users.json", 'utf-8', function(err, data){
            if(err) {
                console.error(err);
                return;
            }
            try{
                const users = JSON.parse(data);
                for (const userData of users){
                    let user = new User(
                        userData.username,
                        userData.password,
                        userData.firstName,
                        userData.lastName,
                        userData.gender,
                        userData.birthDate,
                        userData.role
                    );
                    if (user.role === Role.Costumer) {
                        user.shoppingCart = userData.shoppingCart || [];
                        user.rewardPoints = userData.rewardPoints || 0;
                        user.customerType = userData.customerType || null;
                    } else if (user.role === Role.Manager) {
                        user.rentalAgency = userData.rentalAgency || null;
                    }         
                    
                    this.values.push(user);
                    this.map[user.username] = user;
                }
            } catch(parseError){
                console.error(parseError);
                callback(parseError, null);
            }
        });
    }
}

module.exports = {Costumer, Users, Manager, Admin, User};