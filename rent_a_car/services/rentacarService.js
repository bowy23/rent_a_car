const fs = require("fs");
const rentacars = require("./rentACar");

let rents = [];

async function getRentacars() {
    return new Promise((resolve, reject) => {
        const filePath = "data/rentACars.json";
        fs.readFile(filePath, 'utf8', (err, data) => {
            if (err) {
                console.error("Error reading file:", err);
                reject(err);
                return;
            }

            let rentacarsArray = [];

            try {
                rentacarsArray = JSON.parse(data);

                if (!Array.isArray(rentacarsArray)) {
                    console.error("Existing data is not an array:", rentacarsArray);
                    reject("Existing data is not an array");
                    return;
                }
            } catch (parseError) {
                console.error("Error parsing JSON:", parseError);
                reject(parseError);
                return;
            }

            rents = rentacarsArray;
            resolve(rents);
        });
    });
}

async function getRentacar(name){
    return new Promise((resolve, reject) => {
        const filePath = "data/rentACars.json";
        fs.readFile(filePath, 'utf8', (err, data) => {
            if (err) {
                console.error("Error reading file:", err);
                reject(err);
                return;
            }

            let rentacarsArray = [];

            try {
                rentacarsArray = JSON.parse(data);

                if (!Array.isArray(rentacarsArray)) {
                    console.error("Existing data is not an array:", rentacarsArray);
                    reject("Existing data is not an array");
                    return;
                }
            } catch (parseError) {
                console.error("Error parsing JSON:", parseError);
                reject(parseError);
                return;
            }

            const rent = rentacarsArray.find(rentacar => rentacar.name === name);
            resolve(rent);
        });
    });
}

async function addVehicle(name, vehicle){
    return new Promise((resolve, reject) => {
        const filePath = "data/rentACars.json";
        fs.readFile(filePath, 'utf8', (err, data) => {
            if (err) {
                console.error("Error reading file:", err);
                reject(err);
                return;
            }

            let rentacarsArray = [];

            try {
                rentacarsArray = JSON.parse(data);

                if (!Array.isArray(rentacarsArray)) {
                    console.error("Existing data is not an array:", rentacarsArray);
                    reject("Existing data is not an array");
                    return;
                }
            } catch (parseError) {
                console.error("Error parsing JSON:", parseError);
                reject(parseError);
                return;
            }

            const rent = rentacarsArray.find(rentacar => rentacar.name === name);
            const rentacarIndex = rentacarsArray.findIndex(rentacar => rentacar.name === name);
            const vehicles = rent.vehicles;
            vehicles.push(vehicle.Vehicle);
            rent.vehicles = vehicles;

            if (rentacarIndex !== -1) {
                rentacarsArray[rentacarIndex] = rent;

                fs.writeFile(filePath, JSON.stringify(rentacarsArray), (writeErr) => {
                    if (writeErr) {
                        console.error("Error writing to file:", writeErr);
                    } else {
                        console.log("Rentacar updated successfully!");
                    }
                });
            } else {
                console.error("User not found for update.");
            }

            resolve(rent);
        });
    });
}

async function getRents() {
    try {
        await getRentacars();
        return rents;
    } catch (error) {
        console.error("Error getting rents:", error);
        throw error; // Re-throw the error to be handled by the caller
    }
}

function createObject(rentacar) {
    const filePath = "data/rentACars.json";

    fs.readFile(filePath, 'utf8', (err, data) => {
        if (err) {
            console.error("Error reading file:", err);
            return;
        }

        let rentacarsArray = [];

        try {
            // Parse the existing content as JSON
            rentacarsArray = JSON.parse(data);
            
            // Ensure that rentacarsArray is an array
            if (!Array.isArray(rentacarsArray)) {
                console.error("Existing data is not an array:", rentacarsArray);
                return;
            }
        } catch (parseError) {
            console.error("Error parsing JSON:", parseError);
            return;
        }

        rentacarsArray.push({rentacar: rentacar});

        fs.writeFile(filePath, JSON.stringify(rentacarsArray), (writeErr) => {
            if (writeErr) {
                console.error("Error writing to file:", writeErr);
            } else {
                console.log("Rentacar registered successfully!");
            }
        });
    });
}

async function deleteVehicle(brand, model) {
    return new Promise((resolve, reject) => {
        const filePath = "data/rentACars.json";
        fs.readFile(filePath, 'utf8', (err, data) => {
            if (err) {
                console.error("Error reading file:", err);
                return reject(err);
            }

            let rentacarsArray = [];

            try {
                rentacarsArray = JSON.parse(data);

                if (!Array.isArray(rentacarsArray)) {
                    console.error("Existing data is not an array:", rentacarsArray);
                    return reject("Existing data is not an array");
                }
            } catch (parseError) {
                console.error("Error parsing JSON:", parseError);
                return reject(parseError);
            }

            const rentacarIndex = rentacarsArray.findIndex(rentacar =>
                rentacar.vehicles.some(vehicle => vehicle.brand === brand && vehicle.model === model)
            );

            if (rentacarIndex !== -1) {
                const vehicleIndex = rentacarsArray[rentacarIndex].vehicles.findIndex(vehicle =>
                    vehicle.brand === brand && vehicle.model === model
                );

                if (vehicleIndex !== -1) {
                    rentacarsArray[rentacarIndex].vehicles[vehicleIndex].deleted = true;

                    fs.writeFile(filePath, JSON.stringify(rentacarsArray), (writeErr) => {
                        if (writeErr) {
                            console.error("Error writing to file:", writeErr);
                            return reject(writeErr);
                        } else {
                            console.log("Vehicle deleted successfully!");
                            resolve(rentacarsArray[rentacarIndex].vehicles[vehicleIndex]);
                        }
                    });
                } else {
                    console.error("Vehicle not found:", brand, model);
                    return reject("Vehicle not found");
                }
            } else {
                console.error("Rentacar not found");
                return reject("Rentacar not found");
            }
        });
    })
    .catch((error) => {
        console.error("Unhandled Promise Rejection:", error);
        // Handle the error or log it appropriately
        throw error;
    });
}

async function updateVehicle(vehicle) {
    return new Promise((resolve, reject) => {
        const filePath = "data/rentACars.json";
        fs.readFile(filePath, 'utf8', (err, data) => {
            if (err) {
                console.error("Error reading file:", err);
                return reject(err);
            }

            let rentacarsArray = [];

            try {
                rentacarsArray = JSON.parse(data);

                if (!Array.isArray(rentacarsArray)) {
                    console.error("Existing data is not an array:", rentacarsArray);
                    return reject("Existing data is not an array");
                }
            } catch (parseError) {
                console.error("Error parsing JSON:", parseError);
                return reject(parseError);
            }

            const rentacarIndex = rentacarsArray.findIndex(rentacar =>
                rentacar.vehicles.some(veh => veh.brand === vehicle.brand && veh.model === vehicle.model)
            );

            if (rentacarIndex !== -1) {
                const vehicleIndex = rentacarsArray[rentacarIndex].vehicles.findIndex(veh =>
                    veh.brand === vehicle.brand && veh.model === vehicle.model
                );

                if (vehicleIndex !== -1) {
                    rentacarsArray[rentacarIndex].vehicles[vehicleIndex] = vehicle;

                    fs.writeFile(filePath, JSON.stringify(rentacarsArray), (writeErr) => {
                        if (writeErr) {
                            console.error("Error writing to file:", writeErr);
                            return reject(writeErr);
                        } else {
                            console.log("Vehicle deleted successfully!");
                            resolve(rentacarsArray[rentacarIndex].vehicles[vehicleIndex]);
                        }
                    });
                } else {
                    console.error("Vehicle not found:");
                    return reject("Vehicle not found");
                }
            } else {
                console.error("Rentacar not found");
                return reject("Rentacar not found");
            }
        });
    })
    .catch((error) => {
        console.error("Unhandled Promise Rejection:", error);
        // Handle the error or log it appropriately
        throw error;
    });
}


module.exports = { createObject, getRentacars, getRents, getRentacar, addVehicle, deleteVehicle, updateVehicle};