const fs = require("fs");

class Cart{
    constructor(vehicles, user, price){
        this.vehicles = vehicles;
        this.user = user;
        this.price = price;
    }
}

class Carts{
    constructor(){
        this.values = [];
        this.map = {};
        var self = null;
        fs.readFile("carts.json", 'utf-8', function(err, data){
            if(err){
                console.error(err);
                return;
            }
            try{
                const carts = JSON.parse(data);
                for(const cartData of carts){
                    let cart = new Cart(
                        cartData.vehicles,
                        cartData.user,
                        cartData.price
                    );
                    this.values.push(cart);
                    this.map[cart.vehicles] = cart;
                }
            } catch(parseError){
                console.error(parseError);
                callback(parseError, null);
            }
        });
    }
}

module.exports = {Cart, Carts};