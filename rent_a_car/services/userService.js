const fs = require("fs");
const Users = require("./user");

let logedInUser = new Users.User;
let users = [];

function login(username, password){
    const filePath = "data/users.json";

    fs.readFile(filePath, 'utf8', (err, data) => {
        if (err) {
            console.error("Error reading file:", err);
            return;
        }

        let usersArray = [];

        try {
            usersArray = JSON.parse(data);
            
            if (!Array.isArray(usersArray)) {
                console.error("Existing data is not an array:", usersArray);
                return;
            }
        } catch (parseError) {
            console.error("Error parsing JSON:", parseError);
            return;
        }

        for(const user of usersArray){
            if (user.user.username === username && user.user.password === password) {
                if(user.user.role === 'Costumer')
                    this.logedInUser = new Users.Costumer(user.user.username, user.user.password, user.user.firstName, user.user.lastName, user.user.gender, user.user.birthDate, user.user.role, user.user.allRentals)
                else if(user.user.role === 'Manager')
                    this.logedInUser = new Users.Manager(user.user.username, user.user.password, user.user.firstName, user.user.lastName, user.user.gender, user.user.birthDate, user.user.role, user.user.rentalAgency)
                else if(user.user.role === 'Admin')
                    this.logedInUser = new Users.Admin(user.user.username, user.user.password, user.user.firstName, user.user.lastName, user.user.gender, user.user.birthDate, user.user.role)
                return this.logedInUser;
            }
        }
    });
}

function getLoggedIn(){
    return this.logedInUser;
}

function logout(){
    this.logedInUser = new Users.User;
}

function register(user) {
    const filePath = "data/users.json";

    fs.readFile(filePath, 'utf8', (err, data) => {
        if (err) {
            console.error("Error reading file:", err);
            return;
        }

        let usersArray = [];

        try {
            // Parse the existing content as JSON
            usersArray = JSON.parse(data);
            
            // Ensure that usersArray is an array
            if (!Array.isArray(usersArray)) {
                console.error("Existing data is not an array:", usersArray);
                return;
            }
        } catch (parseError) {
            console.error("Error parsing JSON:", parseError);
            return;
        }

        usersArray.push(user);

        fs.writeFile(filePath, JSON.stringify(usersArray, null, 2), (writeErr) => {
            if (writeErr) {
                console.error("Error writing to file:", writeErr);
            } else {
                console.log("User registered successfully!");
            }
        });
    });
}

function updateUser(user) {
    const filePath = "data/users.json";

    fs.readFile(filePath, 'utf8', (err, data) => {
        if (err) {
            console.error("Error reading file:", err);
            return;
        }

        let usersArray = [];

        try {
            // Parse the existing content as JSON
            usersArray = JSON.parse(data);

            // Ensure that usersArray is an array
            if (!Array.isArray(usersArray)) {
                console.error("Existing data is not an array:", usersArray);
                return;
            }

            // Find the index of the user you want to update
            const userIndex = usersArray.findIndex(user => user.username === user.username);

            if (userIndex !== -1) {
                // Update the user data
                usersArray[userIndex] = user;

                // Write the updated data back to the file
                fs.writeFile(filePath, JSON.stringify(usersArray, null, 2), (writeErr) => {
                    if (writeErr) {
                        console.error("Error writing to file:", writeErr);
                    } else {
                        console.log("User updated successfully!");
                    }
                });
            } else {
                console.error("User not found for update.");
            }
        } catch (parseError) {
            console.error("Error parsing JSON:", parseError);
        }
    });
}

function logout(){
    this.logedInUser = new Users.User;
}

async function getallUsers() {
   return new Promise((resolve, reject) => {
        const filePath = "data/users.json";

        fs.readFile(filePath, 'utf8', (err, data) => {
            if (err) {
                console.error("Error reading file:", err);
                reject(err);
                return;
            }

            let usersArray = [];

            try {
                // Parse the existing content as JSON
                usersArray = JSON.parse(data);

                // Ensure that usersArray is an array
                if (!Array.isArray(usersArray)) {
                    console.error("Existing data is not an array:", usersArray);
                    reject("Existing data is not an array");
                    return;
                }
            } catch (parseError) {
                console.error("Error parsing JSON:", parseError);
                reject(parseError);
                return;
            }

            users = usersArray;
        });
    });
}

async function getUsers(){
    await getallUsers();
    return users
}

module.exports = { register, login, getLoggedIn, logout, updateUser, getUsers};
