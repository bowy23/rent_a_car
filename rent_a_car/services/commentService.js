const fs = require("fs");

let comments = [];

async function getallComments() {
  return new Promise((resolve, reject) => {
    const filePath = "data/comments.json";

    fs.readFile(filePath, "utf8", (err, data) => {
      if (err) {
        console.error("Error reading file:", err);
        reject(err);
        return;
      }

      let commentsArray = [];

      try {
        // Parse the existing content as JSON
        commentsArray = JSON.parse(data);

        // Ensure that commentsArray is an array
        if (!Array.isArray(commentsArray)) {
          console.error("Existing data is not an array:", commentsArray);
          reject("Existing data is not an array");
          return;
        }
      } catch (parseError) {
        console.error("Error parsing JSON:", parseError);
        reject(parseError);
        return;
      }

      comments = commentsArray;
      resolve(comments)
    });
  });
}

async function getComments() {
  comments = await getallComments();
  return comments;
}

async function createComment(comment){
    return new Promise((resolve, reject) => {
        const filePath = "data/comments.json";
        fs.readFile(filePath, "utf8", (err, data) => {
          if (err) {
            console.error("Error reading file:", err);
            reject(err);
            return;
          }
    
          let comments = [];
    
          try {
            comments = JSON.parse(data);
    
            if (!Array.isArray(comments)) {
              console.error("Existing data is not an array:", comments);
              reject("Existing data is not an array");
              return;
            }
          } catch (parseError) {
            console.error("Error parsing JSON:", parseError);
            reject(parseError);
            return;
          }
    
          comments.push(comment.comment);
    
          fs.writeFile(filePath, JSON.stringify(comments), (writeErr) => {
            if (writeErr) {
              console.error("Error writing to file:", writeErr);
              reject(writeErr);
            } else {
              console.log("Vehicle added successfully!");
              resolve(comments);
            }
          });
        });
      });
}

function updateComment(comment) {
  const filePath = "data/comments.json";

  fs.readFile(filePath, 'utf8', (err, data) => {
      if (err) {
          console.error("Error reading file:", err);
          return;
      }

      let commentsArray = [];

      try {
          commentsArray = JSON.parse(data);

          if (!Array.isArray(commentsArray)) {
              console.error("Existing data is not an array:", commentsArray);
              return;
          }
          const commentIndex = commentsArray.findIndex(c => c.text === comment.text)

          if (commentIndex !== -1) {
              commentsArray[commentIndex] = comment;

              fs.writeFile(filePath, JSON.stringify(commentsArray, null, 2), (writeErr) => {
                  if (writeErr) {
                      console.error("Error writing to file:", writeErr);
                  } else {
                      console.log("Comment updated successfully!");
                  }
              });
          } else {
              console.error("Comment not found for update.");
          }
      } catch (parseError) {
          console.error("Error parsing JSON:", parseError);
      }
  });
}

async function getCommentsForObject(rentacarObject) {
  return new Promise((resolve, reject) => {
    const filePath = "data/comments.json";

    fs.readFile(filePath, "utf8", (err, data) => {
      if (err) {
        console.error("Error reading file:", err);
        reject(err);
        return;
      }

      let commentsArray = [];

      try {
        // Parse the existing content as JSON
        commentsArray = JSON.parse(data);

        // Ensure that commentsArray is an array
        if (!Array.isArray(commentsArray)) {
          console.error("Existing data is not an array:", commentsArray);
          reject("Existing data is not an array");
          return;
        }
      } catch (parseError) {
        console.error("Error parsing JSON:", parseError);
        reject(parseError);
        return;
      }

      comments = commentsArray.filter(c => c.rentacarObject.some(r => r === rentacarObject) );
      resolve(comments)
    });
  });
}

module.exports = {getComments, createComment, updateComment, getCommentsForObject};
