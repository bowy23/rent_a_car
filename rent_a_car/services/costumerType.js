const fs = require("fs");

class CostumerType{
    constructor(name, discount, requiredPoints){
        this.name = name;
        this.discount = discount;
        this.requiredPoints = requiredPoints;
    }
}

class CostumerTypes{
    constructor(){
        this.values = [];
        this.map = {};
        var self = this;
        fs.readFile("costumerTypes.json", 'utf-8', function(err, data){
            if(err){
                console.error(err);
                return;
            }
            try{
                const costumerTypes = JSON.parse(data);
                for(const costumerTypeData of costumerTypes){
                    let costumerType = new CostumerType(
                        costumerTypeData.name,
                        costumerTypeData.discount,
                        costumerTypeData.requiredPoints
                    );
                    this.values.push(costumerType);
                    this.map[costumerType.name] = costumerType;
                }
            } catch(parseError){
                console.error(parseError);
                callback(parseError, null);
            }
        });
    }
}
module.exports = {CostumerType, CostumerTypes}